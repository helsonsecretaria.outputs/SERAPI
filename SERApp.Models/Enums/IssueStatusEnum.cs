﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Enums
{
    public enum IssueStatusEnum
    {
        NotConfirmed = 0, // grey
        NotConfirmedWithinTimePeriod =1, //red
        Confirmed = 2, //yellow
        Closed = 3, // green

    }
}
