﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Enums
{
    public enum ReportTypes
    {
        DayReport = 1,
        WeekReport = 2,
        Handelserapport = 3,
        Ryckrapport = 4,
        Oppettidesrapport = 5,
        SkadegorelserKlotterrapport = 6,
        Viten = 7,
        BistroSolna= 8,
        MonthReport =9,
    }
}
