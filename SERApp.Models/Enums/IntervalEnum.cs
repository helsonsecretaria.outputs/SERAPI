﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Enums
{
    public enum IntervalEnum
    {
        Day = 1,
        Month = 2
    }
}
