﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class DeleteResultModel
    {
        public bool IsDeleted { get; set; }
    }
}
