﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IssueSettingsModel
    {
        public int IssueSettingId { get; set; }
        public int StatusInterval { get; set; }
        public int AccountId { get; set; }
    }
}
