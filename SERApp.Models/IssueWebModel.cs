﻿using SERApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class IssueWebModel
    {
        public int IssueWebId { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateChecked { get; set; }
        public DateTime? DateFinished { get; set; }

        public int AccountId { get; set; }

        public int SiteId { get; set; }
        public SiteModel Site { get; set; }

        public int TenantId { get; set; }
        public TenantModel Tenant { get; set; }

        public int IssueType1Id { get; set; }
        public IssueType1Model IssueType1 { get; set; }

        public int IssueType2Id { get; set; }
        public IssueType2Model IssueType2 { get; set; }

        public string Email { get; set; }
        public string Phone { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string ApartmentNumber { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool IsPolice { get; set; }
        public IssueStatusEnum Status { get; set; }
        public int ReminderEmailSentCount { get; set; }

        public List<IssueWebHistoryModel> IssueWebHistories { get; set; }
        public List<Issue_IssueType3Model> IssueType3 { get; set; }
    }
}
