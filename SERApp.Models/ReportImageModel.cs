﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class ReportImageModel
    {
        public int ReportImageId { get; set; }
        public int ReportId { get; set; }
        public string Image { get; set; }

        public ReportModel Report { get; set; }
    }
}
