﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class WeekTypeReportModel
    {
        public string Day { get; set; }
        public Dictionary<string, string> Columns { get; set; }
    }
}
