﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class SummaryFilterModel
    {
        public DateModel date { get; set; }
        public int accountId { get; set; }
        public int siteId { get; set; }
    }
}
