﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models.Common
{
    public class IssueFilter
    {
        public int accountId { get; set; }
        public int siteId { get; set; } = 0;
        public int statusId { get; set; } = -1;
        public int contactId { get; set; } = 0;
        public int issueType1Id { get; set; } = 0;
        public int issueType2Id { get; set; } = 0;
        public int issueType3Id { get; set; } = 0;
        public string searchText { get; set; } = "";
        public DateModel fromDate { get; set; } = null;
        public DateModel toDate { get; set; } = null;
    }
}
