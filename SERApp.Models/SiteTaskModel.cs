﻿using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SiteTaskModel
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public int TaskId { get; set; }
        public string Performer { get; set; }
        public int PerformerId { get; set; }
        public DateModel StartDate { get; set; }
        public string StartDateString { get; set; }
        public string InfoRoles { get; set; }
        public bool IsPublished { get; set; }
        public bool IsCurrentlyPublished { get; set; }
        public DateTime PublishDate { get; set; }

        public SiteModel Site { get; set; }
        public TaskModel Task { get; set; }

        public IEnumerable<UserModel> Users { get; set; }
    }
}
