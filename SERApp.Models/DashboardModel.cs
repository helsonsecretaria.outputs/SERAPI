﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class DashboardModel
    {
        public int AccountsCount { get; set; }
        public int TotalUsersCount { get; set; }
        public int ActiveUsersCount { get; set; }
        public int InActiveUsersCount { get; set; }
        public int SitesCount { get; set; }
        public List<AccountsUserCounterModel> AccountUserCounter { get; set; }
    }

    public class AccountsUserCounterModel {
        public string AccountName { get; set; }
        public int TotalUserCount {get; set;}
    }

    public class DashboardLogModel
    {
        public int InformationCount { get; set; }
        public int WarningCount { get; set; }
        public int ErrorCount { get; set; }
        public int NotifificationCount { get; set; }
    }
}
