﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Models
{
    public class SMSModel
    {

    }
    public class SMSResponseModel {
        //public bool Success { get; set; }
        public int ID { get; set; }
        public int ? BundleID { get; set; } 
        public int Status { get; set; }
        public string StatusDescription { get; set; }
        public string From { get; set; }
        public string Message { get; set; }
        public string To { get; set; }
        public string CountryCode { get; set; }
        public string Currency { get; set; }
        public double TotalPrice { get; set; }
        public double Price { get; set; }
        public string Encoding { get; set; }
        public int Segments { get; set; }
        public int Prio { get; set; }
        public DateTime Created { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime Modified { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
    public class OutgoingSMSModel
    {
        public string From { get; set; }
        public List<string> Numbers { get; set; }
        public List<int> Contacts { get; set; }
        public List<int> Groups { get; set; }
        public string Message { get; set; }
        public int Prio { get; set; }
        public bool Email { get; set; }
    }
}
