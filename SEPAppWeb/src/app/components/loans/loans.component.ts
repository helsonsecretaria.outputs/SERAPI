import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { LoansListComponent } from './loans-list.component';
import { LoansPreviewComponent } from './loans-preview.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { BaseComponent } from '../../core/components';
@Component({
    templateUrl: './loans.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class LoansComponent extends BaseComponent implements OnInit {
    @ViewChild('loansList') loansList: LoansListComponent;
    @ViewChild('loansPreview') loansPreview: LoansPreviewComponent;
    public isAdminOrUserEditor: boolean;
    public isPreviewOn: boolean = false;
    public loanPreviewData: any;
    public listCol = 'col-md-12';
    constructor(
        public router: Router,
        private route: ActivatedRoute,
        private location: Location) {
        super(location, router, route)
    }
    public ngOnInit() {
        let currentUser = super.currentLoggedUser();
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.loansList.isAdminOrUserEditor = true;
            this.loansList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.loansList.isSuperAdmin = true;
            let secondaryUserData = super.secondaryAdminUser();
            if(secondaryUserData){
                this.loansList.loadDataById(secondaryUserData.AccountId);
            }
        }
    }

    public loadLoanPreview(data : any){
        this.loansPreview.loadLoanPreview(data);
    }
}
