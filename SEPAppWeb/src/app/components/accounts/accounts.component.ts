import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { AccountsListComponent } from './accounts-list.component';

@Component({
    selector: 'app-accounts',
    templateUrl: './accounts.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class AccountsComponent implements OnInit {
    isAdminOrUserEditor: boolean;
    @ViewChild('accountsList') usersList: AccountsListComponent;
    constructor() {}
    public ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.usersList.editAccount(currentUser.accountId)
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
            this.usersList.editAccount(currentUser.accountId)
        }
    }
}
