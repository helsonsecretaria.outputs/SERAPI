import { Component, OnInit } from '@angular/core';
import { CustomTypeService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { CustomTypeModel } from '../../models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-custom-type-create',
  templateUrl: './custom-type-create.component.html',
  styleUrls: ['./custom-type-create.component.scss']
})
export class CustomTypeCreateComponent implements OnInit {
  model : any;
  public showSpinner: boolean = false;
  constructor(private customTypeService: CustomTypeService, 
    private route: ActivatedRoute,
    private toastr: ToastsManager,
    private router: Router) { }

  ngOnInit() {
    this.model = new CustomTypeModel();
  }

  public saveChanges() : void{
    this.showSpinner = true;
    this.customTypeService.saveCustomType(this.model)
    .catch((err: any) => {
        this.toastr.error(err, 'Error');
        return Observable.throw(err);
    }).finally(() => {
         setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response) => {
        if(response.ErrorCode)
        {
            this.toastr.error(response.Message, "Error"); 
        }
        else {
            this.toastr.success(response.Message, "Success"); 
        }
        this.ngOnInit();
    });
}

}
