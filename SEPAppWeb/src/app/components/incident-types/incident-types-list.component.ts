import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { IncidentTypeService } from '../../services/incident-type.service';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

@Component({
  selector: 'app-incident-types-list',
  templateUrl: './incident-types-list.component.html',
  styleUrls: ['./incident-types-list.component.scss']
})
export class IncidentTypesListComponent implements OnInit {
  public showSpinner: boolean = false;
  public itemPerPage: number = 10;
  public currentPage: number = 1;
  public totalItems: number = 10;
  public isAdminOrUserEditor = false;
  public isSuperAdmin = false;5
  public isPreviewOn: boolean = false;
  //public parentComponent : any;
  constructor(
    //private parentInj: Injector,
    private router: Router,
    private route: ActivatedRoute, 
    private toastr: ToastsManager,
    private incidentTypeService: IncidentTypeService) 
    {
     // this.parentComponent = this.parentInj.get(IncidentTypesComponent);
    }
    packageData: Subject<any> = new Subject<any[]>();  

  ngOnInit() 
  {
    if(this.isSuperAdmin)
    {        
        this.loadData();
    }
  }

  public loadData(page?: number, id?: number){
    this.showSpinner = true;
    let filter = '';
    if (page != null) {
        this.currentPage = page; 
    } 

    this.incidentTypeService.getAllIncidentTypes(this.currentPage, this.itemPerPage, filter)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
       // this.toastr.success(response.Message, "Success"); 
    });    
  }

  public loadDataById(id:number){
    this.showSpinner = true;
     this.incidentTypeService.getAllIncidentTypes(this.currentPage, this.itemPerPage, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : IPagedResults<any>) => {
        this.packageData.next(response.Data);
        this.totalItems = response.Data.length;
       // this.toastr.success(response.Message, "Success"); 
    }); 
}

public editIncidentType(id:number){
  
    this.router.navigate([`${id}/edit`], { relativeTo: this.route });  
}


}
