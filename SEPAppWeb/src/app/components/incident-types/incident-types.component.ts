import { Component, OnInit, ViewChild } from '@angular/core';
import { IncidentTypesListComponent } from './incident-types-list.component';

@Component({
  selector: 'app-incident-types',
  templateUrl: './incident-types.component.html',
  styleUrls: ['./incident-types.component.scss']
})
export class IncidentTypesComponent implements OnInit {
  @ViewChild('incidentTypeList') incidentTypeList: IncidentTypesListComponent;
  public isAdminOrUserEditor: boolean;
  public isPreviewOn: boolean = false;
  public loanPreviewData: any;
  public listCol = 'col-md-12';
  constructor() { }

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'))
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.incidentTypeList.isAdminOrUserEditor = true;
            this.incidentTypeList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.incidentTypeList.isSuperAdmin = true;
        }
  }
}
