import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

import { CommonService } from '../../services';
@Component({
    selector: 'app-recent-log-list',
    templateUrl: './recent-logs-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class RecentLogListComponent implements OnInit {
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public model: any;
    public dataList: any;
    constructor(private router: Router,
        private route:ActivatedRoute, 
        private commonService: CommonService) {}
    public ngOnInit() { 
        this.loadData();
    }
    public loadData(page?: number){
        this.commonService.getMostRecentLogs()
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    //this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : any) => {
                this.dataList = response.Data;
            });
    }
}