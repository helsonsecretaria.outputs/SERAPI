import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { IPagedResults } from '../../core/interfaces';

import { CommonService } from '../../services';
@Component({
    selector: 'app-error-log-list',
    templateUrl: './error-logs-list.component.html',
    styleUrls: ['../layout/layout.component.scss']
})
export class ErrorLogListComponent implements OnInit {
    public itemPerPage: number = 10;
    public currentPage: number = 1;
    public totalItems: number = 10;
    public model: any;
    public dataList: any;
    constructor(private router: Router,
        private route:ActivatedRoute, 
        private commonService: CommonService) {}

    packageData: Subject<any> = new Subject<any[]>(); 
    public ngOnInit() { 
        this.loadData();
    }
     
    public loadData(page?: number){
        let filter = '';
        if (page != null) {
            this.currentPage = page; 
        } 
        this.commonService.getErrorLogs()
            .catch((err: any) => {
                return Observable.throw(err);
            })
            .finally(() => {
                 setTimeout(() => {
                    //this.showSpinner = false;
                }, 1000);
            })
            .subscribe((response : IPagedResults<any>) => {
                this.packageData.next(response.Data);
                this.totalItems = response.Data.length;
            });
    }
}