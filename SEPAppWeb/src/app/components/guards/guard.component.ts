import { Component, OnInit, ViewChild } from '@angular/core';
import { GuardListComponent } from './guard-list.component';
import { BaseComponent } from '../../core/components';

import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-guard',
  templateUrl: './guard.component.html',
  styleUrls: ['./guard.component.scss']
})
export class GuardComponent extends BaseComponent implements OnInit {
  @ViewChild('guardList') guardList: GuardListComponent;
  public isAdminOrUserEditor: boolean;
  public isPreviewOn: boolean = false;
  public loanPreviewData: any;
  public listCol = 'col-md-12';
  constructor(
        public router: Router,
        private route: ActivatedRoute,
        private location: Location) {
        super(location, router, route) 
  }

  ngOnInit() {
    //let currentUser = JSON.parse(localStorage.getItem('currentUser'))
    let currentUser = super.currentLoggedUser();
        if(currentUser.role === 'admin')
        {
            this.isAdminOrUserEditor = true;
            this.guardList.isAdminOrUserEditor = true;
            this.guardList.loadDataById(currentUser.accountId);
        }
        else if(currentUser.role === 'user')
        {
            this.isAdminOrUserEditor = false;
        }
        else if(currentUser.role === 'superadmin')
        {
            this.guardList.isSuperAdmin = true;
            let secondaryUserData = super.secondaryAdminUser();
            if(secondaryUserData){
                this.guardList.loadDataById(secondaryUserData.AccountId);
            }
        }
  }

}
