import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, Event as RouterEvent, NavigationStart, NavigationEnd, 
    NavigationCancel, NavigationError, ActivatedRoute 
} from '@angular/router'; 

import { ToastsManager } from 'ng2-toastr';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    constructor(private toastr: ToastsManager, private vRef: ViewContainerRef,
    private router: Router, private activatedRoute: ActivatedRoute) {
        this.toastr.setRootViewContainerRef(vRef)
    }

    ngOnInit() {}
}
