import { Component, OnInit } from '@angular/core';
import {GrowlModule,Message} from 'primeng/primeng';
import { TagLogModel } from '../../models';
import { TagLogService } from '../../services/tag-log.service';
import { GuardService } from '../../services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-tag-logs-create',
  templateUrl: './tag-logs-create.component.html',
  styleUrls: ['./tag-logs-create.component.scss']
})
export class TagLogsCreateComponent implements OnInit {
  showSpinner: boolean;
  Guards: any[];
  isAdminOrUserEditor: boolean;
  currentUser: any;
  photoSrc: string;
  model : any;
  constructor(private guardService: GuardService, private tagLogsService: TagLogService) 
  {

  }

  ngOnInit() {
    this.setDefaults();
    this.model = new TagLogModel(); 
    this.model.GuardId = 0;
    this.loadGuards();
  }


loadGuards()
{
    this.Guards = [];
    this.guardService.getAllGuards(null,null, null)
    .catch((err: any) => {
        return Observable.throw(err);
    })
    .finally(() => {
            setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    })
    .subscribe((response : any) => {
        
        this.Guards = response.Data;
        //this.toastr.success(response.Message, "Success"); 
    }); 
};

  public setDefaults(){
    let now = new Date();
    this.photoSrc = "/assets/images/upload-empty.png";
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.role === 'admin')
    {
        this.isAdminOrUserEditor = true;
    }
    else if(this.currentUser.role === 'user')
    {
        this.isAdminOrUserEditor = false;
    }
}
}
