import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService, BaseDataService } from '../core/services';
import { API_URL }  from '../constants';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IPagedResults } from '../core/interfaces';

@Injectable()
export class UserModuleRoleService extends BaseDataService {
    constructor(protected dataService: DataService) {
        super(dataService, API_URL);
    }
    

    public GetUserModuleRolesById(id: number): Observable<any>{
        let url = `UserModuleRole/GetByUserId?id=${id}`;    
        return super.getAll<any>(url);
    }


    public save(data){
        let url = `UserModuleRole/Save`;
        return super.save<any>(url,data);
    }

    public delete(data){
         let url = `UserModuleRole/Delete`;
        return super.save<any>(url,data);
    }
}
