export class TenantsModel {
    public Name : number;
    public Email : string;
    public Phone : string;
    public PermiseType : number = 0;
    public VisitingAddress : string;
    public UserName : string;
    public Password : string;
    public InsuranceCompany : string;
    public SecurityCompany : string;
    public SiteManagerName : string;
    public SiteManagerMobilePhone : string;
    public SiteManagerPhone : string;
    public ContractName : string;
    public ContractVatNo : string;
    public InvoiceAddress : string;
    public InvoiceZipAndCity : string;
    public ContactComments : string;
    public CCTV : boolean = false;
    public IntrusionAlarm : boolean = false;
    public SiteManagerEmail : string;
    public Flag : boolean = false;
    public PropertyDesignation: string;
    public UseWrittenStatement : boolean = false;
}