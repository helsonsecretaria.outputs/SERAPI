import { SiteModel } from ".";
import { IncidentGuardModel } from "./incident-guard-model";

export class GuardModel {
    public Id : number;
    public Name : string;
    public CreatedDate: Date;
    public LastUpdatedDate: Date;

    public SiteId: number;
    public Site: SiteModel;
    public IncidentGuard: IncidentGuardModel[];
}
