import { IncidentModel } from "./incident-model";
import { GuardModel } from "./guard-model";

export class IncidentGuardModel {
    public Id: number;
    public IncidentId: number;
    public Incident: IncidentModel;
    public GuardId: number;
    public Guard: GuardModel;
}
