export interface IPagedResults<T> {
    TotalRecords: number;
    Message: string;
    StatusCode: number;
    Success: boolean;
    Data: T;
}