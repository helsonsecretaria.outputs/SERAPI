﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;


namespace SERApp.Repository.Repositories
{
    public class SettingRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public SettingRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Setting Get(int id) {
            return db.Settings.Where(s => s.SettingId == id).SingleOrDefault();
        }


        public Setting Get(int configSettingId, int accountId) {
            return db.Settings.Where(s => s.ConfigSettingId == configSettingId && s.AccountId == accountId).SingleOrDefault();
        }

        public Setting Get(string name, int accountId) {
            return db.Settings.Where(s => s.Name == name && s.AccountId == accountId).SingleOrDefault();
        }

        public void Save(AccountSettingModel model)
        {
            var data = db.Settings.Where(s => s.AccountId == model.AccountId && s.ConfigSettingId == model.ConfigSettingId).SingleOrDefault();
            if (data != null)
            {
                data.Value = model.Value;
            }
            else
            {
                var newSetting = new Setting()
                {
                    AccountId = model.AccountId,
                    ConfigSettingId = model.ConfigSettingId,
                    Name = model.Name,
                    Value = model.Value
                };
                db.Settings.Add(newSetting);
            }
            db.SaveChanges();
        }

        public List<Setting> GetAccountSettings(int accountId) {
            return db.Settings.Where(a => a.AccountId == accountId).ToList();
        }

        public List<ConfigSetting> GetConfigSettings() {
            return db.ConfigSettings.ToList();
        }

        public ConfigSetting GetConfigSetting(int id) {
            return db.ConfigSettings.Where(c => c.Id == id).SingleOrDefault();
        }

        public ConfigSetting GetConfigSetting(string name)
        {
            return db.ConfigSettings.Where(c => c.SettingName == name).SingleOrDefault();
        }
    }
}
