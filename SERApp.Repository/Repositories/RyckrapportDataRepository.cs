﻿using SERApp.Data.Models;
using SERApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace SERApp.Repository.Repositories
{
    public class RyckrapportDataRepository : SERApp.Repository.Repositories.Repository<RyckrapportData>, IRepository<RyckrapportData>, IRyckrapportDataRepository
    {
        public IEnumerable<RyckrapportData> GetAllRyckrapportChildIncluded()
        {
            var data = dbContext.Set<RyckrapportData>()
                .Include(x=>x.Tenant)
                .Include(x=>x.RyckrapportDataSections)
                .Include(x=>x.RyckrapportDataSections.Select(t=>t.Section));
             //.Include(x => x.RyckrapportDataSections)
            // .Include(x => x.RyckrapportDataSections.Select(r => r.Section));
             //.Include(x=>x.Report);
            // .Include(x => x.IncidentGuard);
            return data;
        }
    }
}
