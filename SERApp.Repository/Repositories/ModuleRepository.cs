﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SERApp.Repository.Repositories
{
    public class ModuleRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings[ConfigSettings.SERAppEntitiesKey].ConnectionString;
        private SERAppDBContext db;
        public ModuleRepository()
        {
            db = new SERAppDBContext(connString);
        }

        public Module Get(int id)
        {
            return db.Modules
                .Where(c => c.Id == id)
                .SingleOrDefault();
        }

        public List<Module> GetAll()
        {
            return db.Modules
                .Where(m => m.IsActive == true)
                .ToList();
        }

        public List<Module> GetAllActive()
        {
            return db.Modules
                .Where(a => a.IsActive == true).ToList();
        }
    }
}
