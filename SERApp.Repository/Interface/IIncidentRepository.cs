﻿using SERApp.Data.Models;
using SERApp.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IIncidentRepository : IRepository<Incident>
    {
        IEnumerable<Incident> GetAllIncidentsChildIncluded(int siteId);
        Incident GetWithChildsIncluded(int id);
        IEnumerable<IncidentListModel> GetAllForList(int siteId);
    }
}
