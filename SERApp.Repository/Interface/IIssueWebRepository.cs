﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IIssueWebRepository : IRepository<IssueWeb>
    {
        IEnumerable<IssueWeb> GetAllIncludingChildren(int accountId);
        IssueWeb GetInlcudingChildrenId(int id);
        void AddIssueWebHistory(IssueWebHistory data);
        List<IssueWebHistory> GetIssueWebHistoryByIssueWebId(int IssueWebId);
    }
}
