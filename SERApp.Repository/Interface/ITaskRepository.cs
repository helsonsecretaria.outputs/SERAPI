﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SERApp.Repository.Interface
{
    public interface ITaskRepository : IRepository<Task>
    {
        IEnumerable<Task> GetAllTasksChildIncluded(int accountId);
        IEnumerable<Task> GetAllTasksSiteTaskIncluded();
        Task GetWithChildsIncluded(int id);
    }
}
