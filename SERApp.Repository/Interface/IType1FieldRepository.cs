﻿using SERApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Repository.Interface
{
    public interface IType1FieldRepository : IRepository<Type1Field>
    {
        List<Type1Field> FindByTypeId(int id);
    }
}
