﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SERApp.API.Models
{
    public class TagLogCreateModel
    {
        public string RFID { get; set; }
        public int GuardId { get; set; }
    }
}