﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.API.Models.ResponseModels
{
    public class ResponseDataModel<T> : ResponseModel where T : class
    {
        /// <summary>
        /// Contains the returned property of type T.
        /// </summary>
        public T Data { get; set; }
        public T result { get; set; }
    }
}
