import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from '../../../../services';

import { Observable } from 'rxjs/Observable';
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    pushRightClass: string = 'push-right';
    public currentUser: any;
    constructor(
        private authService: AuthenticationService,
        private translate: TranslateService, 
        public router: Router) {

        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = currentUser;
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        let model = {
            userId : this.currentUser.id
        };
        this.authService.logoff(model)
        .catch((err: any) => {
            return Observable.throw(err);
        }).finally(() => {

        })
        .subscribe((response) => { 
            localStorage.removeItem('currentUser');
            localStorage.removeItem('isLoggedin');
        });
        
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    editUser(){
        let id = this.currentUser.id;
        if(id > 0)
        {
            this.router.navigate([`users/${id}/edit`]);  
        }
    }
}
