﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Constants;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class EmailService
    {
        EmailRepository _emailRepository;
        LogRepository _logRepository;
        public EmailService() {
            _emailRepository = new EmailRepository();
            _logRepository = new LogRepository();
        }

        public EmailTemplateModel GetEmailTemplate(int id)
        {
            try
            {
                var data = _emailRepository.GetEmailTemplate(id);
                return entityToModel(data);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }

        public AccountEmailTemplateModel GetAccountEMailTemplate(int id)
        {
            try
            {
                var data = _emailRepository.GetAccountEmailTemplate(id);
                return entityToModel(data);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Get, 0, 0);
                throw ex;
            }
        }
        //@TODO:Depricate EmailTemplate table and use EmailTemplateTranslation instead
        //public List<EmailTemplateModel> GetAllEmailTemplate() {
        //    var data = _emailRepository.GetAllEmailTemplates().ToList();
        //    return data.Select(e => new EmailTemplateModel()
        //    {
        //        Id = e.Id,
        //        RoutineName = e.RoutineName,
        //        Description = e.Description,
        //        Subject = e.Subject,
        //        Body = e.Body,
        //        IsSystem = e.IsSystem
        //    }).ToList();
        //}

        public List<EmailTemplateTranslationModel> GetAllEmailTemplate(int languageId = 1)
        {
            var data = (from et in _emailRepository.GetAllEmailTemplates()
                        join etl in _emailRepository.GetAllEmailTemplatesByLanguage(languageId)
                        on et.Id equals etl.EmailTemplateId
                        join l in _emailRepository.GetAllLanguages()
                        on etl.LanguageId equals l.Id
                        select new { et, etl, l }).ToList();
            return data.Select(e => new EmailTemplateTranslationModel()
            {
                Id = e.et.Id,
                LanguageId = e.etl.LanguageId,
                LanguagenName = e.l.Name,
                RoutineName = e.et.RoutineName,
                Description = e.et.Description,
                Subject = e.etl.Subject,
                Body = e.etl.Body,
                IsSystem = e.et.IsSystem
            }).ToList();
        }

        public List<EmailTemplateModel> GetAllNonSystemEmailTemplates()
        {
            var data = _emailRepository.GetAllNonSystemEmailTemplates().ToList();
            return data.Select(e => new EmailTemplateModel()
            {
                Id = e.Id,
                RoutineName = e.RoutineName,
                Description = e.Description,
                Subject = e.Subject,
                Body = e.Body,
                IsSystem = e.IsSystem
            }).ToList();
        }

        public List<AccountEmailTemplateModel> GetAllAccountEmailTemplate(int id){
            var data = _emailRepository.GetAllAccountEmailTemplateById(id)
                .Select(e => new {
                    Id = e.Id,
                    AccountId = e.AccountId,
                    EmailTemplateId = e.EmailTemplateId,
                    Subject = e.Subject,
                    Body = e.Body,
                    RoutineName = _emailRepository.GetEmailTemplate(e.EmailTemplateId).RoutineName,
                    Description = _emailRepository.GetEmailTemplate(e.EmailTemplateId).Description,
                })
                .ToList();

            var accountTemplates = GetAllNonSystemEmailTemplates()
                .Select(e => new {
                    Id = 0,
                    AccountId = 0,
                    EmailTemplateId = e.Id,
                    Subject = e.Subject,
                    Body = e.Body,
                    RoutineName = e.RoutineName,
                    Description = e.Description,
                })
                .ToList();

            var merged = data.Union(accountTemplates)
                .GroupBy(a => a.EmailTemplateId, a => new
                {
                    Id = a.Id,
                    AccountId = a.AccountId,
                    EmailTemplateId = a.EmailTemplateId,
                    Subject = a.Subject,
                    Body = a.Body,
                    RoutineName = a.RoutineName,
                    Description = a.Description,
                })
                .ToList();

            return merged.Select(e => new AccountEmailTemplateModel()
            {
                Id = e.FirstOrDefault().Id,
                RoutineName = e.FirstOrDefault().RoutineName,
                Description = e.FirstOrDefault().Description,
                AccountId = e.FirstOrDefault().AccountId,
                EmailTemplateId = e.FirstOrDefault().EmailTemplateId,
                Subject = e.FirstOrDefault().Subject,
                Body = e.FirstOrDefault().Body
            }).ToList();
        }

        public void SaveEmailTemplate(EmailTemplateModel model) {
            try {
                _emailRepository.SaveEmailTemplate(model);
            }
            catch (Exception ex) {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public void SaveAccountEmailTemplate(AccountEmailTemplateModel model) {
            try {
                _emailRepository.SaveAccountEmailTemplate(model);
            }
            catch (Exception ex)
            {
                _logRepository.LogException(ex, LogMessagingSettings.Save, 0, 0);
                throw ex;
            }
        }

        public AccountEmailTemplateModel GetAccountEmailTemplate(string routineName, int accountId) {
            var data = _emailRepository.GetAccountEmailTemplate(routineName, accountId);
            return entityToModel(data);
        }

        public EmailTemplateModel entityToModel(EmailTemplate entity) {
            return new EmailTemplateModel() {
                Id = entity.Id,
                RoutineName = entity.RoutineName,
                Description = entity.Description,
                Subject = entity.Subject,
                Body = entity.Body,
                IsSystem = entity.IsSystem
            };
        }

        public AccountEmailTemplateModel entityToModel(AccountEmailTemplate entity) {
            return new AccountEmailTemplateModel() {
                Id = entity.Id,
                AccountId = entity.AccountId,
                EmailTemplateId = entity.EmailTemplateId,
                Subject = entity.Subject,
                Body = entity.Body
            };
        }
    }
}
