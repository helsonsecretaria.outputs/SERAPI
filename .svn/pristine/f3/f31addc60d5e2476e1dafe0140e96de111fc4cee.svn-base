﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface ITagService
    {
        IEnumerable<TagModel> GetTags();
        bool AddTag(TagModel model);
        void DeleteTagById(int id);
        TagModel GetTagById(int id);
    }
    public class TagService : ITagService
    {
        //private IRepository<Tag> _repository;
        private ITagRepository _repository;
        public TagService()
        {
            _repository = new TagRepository();
        }
        public bool AddTag(TagModel model)
        {
            try
            {

                if (model.Id > 0)
                {
                    var existingTag = _repository.Get(model.Id);
                    existingTag.RFIDData = model.RFIDData;
                    existingTag.TenantID = model.TenantID;
                    existingTag.Description = model.Description;

                    _repository.Update(existingTag);
                }
                else
                {
                    _repository.Save(new Tag()
                    {
                        Description = model.Description,
                        RFIDData = model.RFIDData,
                        TenantID = model.TenantID
                    });
                }

                
                return true;
            } catch(Exception ex)
            {
                return false;
            }       
        }

        public void DeleteTagById(int id)
        {
            _repository.Delete(id);
        }

        public TagModel GetTagById(int id)
        {
            var existingData = _repository.GetTagIncludeChilds(id);
            return new TagModel()
            {
                Description = existingData.Description,
                Id = existingData.Id,
                RFIDData = existingData.RFIDData,
                TenantID = existingData.TenantID,
                SiteId = existingData.Tenant.SiteId
            };
        }

        public IEnumerable<TagModel> GetTags()
        {
            return _repository.GetAllTagsIncludeChilds().Select(x => new TagModel()
            {
                Description = x.Description,
                Id = x.Id,
                RFIDData = x.RFIDData,
                TenantID = x.TenantID,
                Tenant = new TenantModel()
                {
                    Id = x.Tenant.Id,
                    Name = x.Tenant.Name
                }
            }).ToList();
        }
    }
}
