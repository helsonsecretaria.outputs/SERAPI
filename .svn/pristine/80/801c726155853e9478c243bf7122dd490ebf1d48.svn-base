﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Models.Enums;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueWebService
    {
        IEnumerable<IssueWebModel> GetIssueWeb(int accountId, int siteId =0);
        void SaveIssueWeb(IssueWebModel model, bool isConfirm = false);
        void SaveIssueWebLog(IssueWebHistoryModel model);
        void DeleteIssueWeb(int id);
        IssueWebModel GetById(int id);
        void sendReminder(IssueWebModel model);
        IssueSettingsModel GetIssueSettingByAccountId(int accountId);
    }

    public class IssueWebService : IIssueWebService
    {
        private int defaulSettingtInterval = 1;

        private readonly IIssueWebRepository _repository;
        private readonly AccountService _accountService;
        private readonly IIssueRecipientRepository _recipientRepository;
        private readonly EmailRepository _emailRepository;
        private readonly IIssueSettingsRepository _settingsRepository;
    
        public IssueWebService()
        {
            _repository = new IssueWebRepository();
            _accountService = new AccountService();
            _recipientRepository = new IssueRecipientRepository();
            _emailRepository = new EmailRepository();
            _settingsRepository = new IssueSettingsRepository();
        }

        public IssueSettingsModel GetIssueSettingByAccountId(int accountId)
        {
            var data = _settingsRepository.GetByAccountId(accountId);

            if (data == null)
            {
                _settingsRepository.Save(new IssueSettings()
                {
                    AccountId = accountId,
                    StatusInterval = defaulSettingtInterval
                });

                data = _settingsRepository.GetByAccountId(accountId);
            }


            return new IssueSettingsModel()
            {
                AccountId = data.AccountId,
                IssueSettingId = data.IssueSettingId,
                StatusInterval = data.StatusInterval
            };
        }

        public void DeleteIssueWeb(int id)
        {
            _repository.Delete(id);
        }

        public IssueWebModel GetById(int id)
        {
            var data = _repository.GetInlcudingChildrenId(id);

            data.IssueWebHistories = _repository.GetIssueWebHistoryByIssueWebId(data.IssueWebId).OrderByDescending(x=>x.IssueWebHistoryId).ToList();
            
            return new IssueWebModel()
            {
                IssueWebId = data.IssueWebId,
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                },
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                },
                ReminderEmailSentCount = data.ReminderEmailSentCount,
                IssueWebHistories = data.IssueWebHistories != null || data.IssueWebHistories.Any() ? data.IssueWebHistories.Select(x => new IssueWebHistoryModel()
                {
                    IssueWebHistoryId = x.IssueWebHistoryId,
                    Date = new SERApp.Models.Common.DateModel() {
                        day = x.Date.Day,
                        month = x.Date.Month,
                        year = x.Date.Year,
                    },
                    Time = new SERApp.Models.Common.TimeModel() {
                        hour = x.Date.Hour,
                        minute = x.Date.Minute,
                        second = x.Date.Second
                    },
                    DateTime = x.Date,
                    //Description = x.Description,
                    DescriptionExternal = x.DescriptionExternal,
                    DescriptionInternal = x.DescriptionInternal,
                    IsDamage = x.IsDamage,
                    IsRent = x.IsRent,
                    IsWarranty = x.IsWarranty,
                    MaterialCost = x.MaterialCost,
                    Minutes = x.Minutes,
                    ReactedBy =x.ReactedBy,
                    IssueWebId = x.IssueWebId,
                    IssueWebHistoryType = x.IssueWebHistoryType
                }).ToList() : new List<IssueWebHistoryModel>(),
                
            };
        }

        public IEnumerable<IssueWebModel> GetIssueWeb(int accountId, int siteId= 0)
        {
            var dataList = _repository.GetAllIncludingChildren(accountId);

            if (siteId != 0)
            {
                dataList = dataList.Where(x => x.SiteId == siteId).ToList();
            }

            var toCheck = dataList.Where(x => x.Status == 0).ToList();
            var accountInterval = GetIssueSettingByAccountId(accountId);

            toCheck.ForEach(x => 
            {
                var dateMax = x.DateCreated.AddHours(accountInterval.StatusInterval);

                var dateNow = DateTime.Now;

                if (dateMax < dateNow)
                {
                    x.Status = 1;
                    _repository.Update(x);
                }
            });

            return
                dataList.Select(data => new IssueWebModel()
            {
                IssueWebId = data.IssueWebId,
                DateCreated = data.DateCreated,
                DateChecked = data.DateChecked,
                DateFinished = data.DateFinished,
                AccountId = data.AccountId,
                SiteId = data.SiteId,
                TenantId = data.TenantId,
                IssueType1Id = data.IssueType1Id,
                IssueType2Id = data.IssueType2Id,
                Email = data.Email,
                Phone = data.Phone,
                ContactName = data.ContactName,
                Address = data.Address,
                ApartmentNumber = data.ApartmentNumber,
                Image = data.Image,
                Description = data.Description,
                IsPolice = data.IsPolice,
                Status = (IssueStatusEnum)data.Status,
                Site = new SiteModel()
                {
                    Id = data.Site.Id,
                    Name = data.Site.Name
                },
                Tenant = new TenantModel()
                {
                    Id = data.Tenant.Id,
                    Name = data.Tenant.Name
                },
                IssueType1 = new IssueType1Model()
                {
                    IssueType1Id = data.IssueType1.IssueType1Id,
                    Name = data.IssueType1.Name,

                },
                IssueType2 = new IssueType2Model()
                {
                    IssueType2Id = data.IssueType2.IssueType2Id,
                    Name = data.IssueType2.Name,
                },
                    ReminderEmailSentCount = data.ReminderEmailSentCount
                });
        }

        public void SaveIssueWeb(IssueWebModel model, bool isConfirm = false)
        {
            var data = new IssueWeb();
            if (model.IssueWebId == 0)
            {
                data = new IssueWeb()
                {
                    //IssueWebId = model.IssueWebId,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    DateCreated = DateTime.Now,
                    Status = (int)IssueStatusEnum.NotConfirmed,
                    ReminderEmailSentCount = 0
                };

                _repository.Save(data);                
            }
            else {
                data = new IssueWeb()
                {
                    DateCreated = model.DateCreated,
                    IssueWebId = model.IssueWebId,
                    AccountId = model.AccountId,
                    SiteId = model.SiteId,
                    TenantId = model.TenantId,
                    IssueType1Id = model.IssueType1Id,
                    IssueType2Id = model.IssueType2Id,
                    Email = model.Email,
                    Phone = model.Phone,
                    ContactName = model.ContactName,
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    Image = model.Image,
                    Description = model.Description,
                    IsPolice = model.IsPolice,
                    DateChecked = model.DateChecked,
                    DateFinished = model.DateFinished,
                    Status = (int)model.Status
                };

                _repository.Update(data);
            }

            data =_repository.GetInlcudingChildrenId(data.IssueWebId);



            if (isConfirm)
            {
                _repository.AddIssueWebHistory(new IssueWebHistory()
                {
                    Date = DateTime.Now,
                    DescriptionExternal = "Technician confirmed the issue",
                    DescriptionInternal = "Technician confirmed the issue",
                    ReactedBy = "Technician",
                    IssueWebId = data.IssueWebId,
                    //to do add this to enum , confirm = 1, reminder =2
                    IssueWebHistoryType = "success"
                });

                var creatorTemplate = _emailRepository.GetEmailTemplateFromRoutineName("ConfirmIssue");

                Service.Tools.Email.SendEmailWithAttachment("", data.Email, creatorTemplate.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                       creatorTemplate.Body,
                       "", data.Image, null, true);

                //var recipients = _recipientRepository.GetAllRecipients(data.SiteId).Where(x => x.IssueRecipientTypeId == 1).ToList();
                //string AppUrl = ConfigurationManager.AppSettings["appurl"];

                //recipients.ForEach(recipient =>
                //{
                //    var routineName = "ConfirmIssue";
                //    var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

                //    if (!string.IsNullOrEmpty(recipient.Email))
                //    {
                //        Service.Tools.Email.SendEmailWithAttachment("", recipient.Email, template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                //        template.Body,
                //        "", data.Image, null, false);
                //    }

                //    if (!string.IsNullOrEmpty(recipient.Mobile))
                //    {
                //        var smsService = new SMS();
                //        smsService.SendSMS("SER4 Application", recipient.Mobile, template.SMSBody
                //            .Replace("{appurl}", AppUrl)
                //            .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();
                //    }

                //});


            }
            else {

                var recipients = _recipientRepository.GetAllRecipients(data.SiteId).Where(x => x.IssueRecipientTypeId == 1).ToList();
                string AppUrl = ConfigurationManager.AppSettings["appurl"];

                recipients.ForEach(recipient =>
                {
                    var routineName = Regex.Replace(recipient.IssueRecipientType.Name, @"\s+", "");
                    var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

                    if (!string.IsNullOrEmpty(recipient.Email))
                    {
                        Service.Tools.Email.SendEmailWithAttachment("", recipient.Email, template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                        template.Body.Replace("{note}", model.IssueWebId == 0 ? "A new issue has been created" : "An issue has been updated")
                            .Replace("{object}", data.Site.Name)
                            .Replace("{tenant}", data.Tenant.Name)
                            .Replace("{type1}", data.IssueType1.Name)
                            .Replace("{phone}", data.Phone)
                            .Replace("{contactname}", data.ContactName)
                            .Replace("{address}", data.Address)
                            .Replace("{email}", data.Email)
                            .Replace("{apartment}", data.ApartmentNumber.ToString())
                            .Replace("{type}", data.IssueType2.Name)
                            .Replace("{description}", data.Description)
                            .Replace("{ispolice}", data.IsPolice.ToString())
                            .Replace("{appurl}", AppUrl)
                            .Replace("{IssueWebId}", data.IssueWebId.ToString()),
                        "", data.Image, null, true);
                    }

                    if (!string.IsNullOrEmpty(recipient.Mobile))
                    {
                        var smsService = new SMS();
                        smsService.SendSMS("SER4 Application", recipient.Mobile, template.SMSBody
                            .Replace("{appurl}", AppUrl)
                            .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();
                    }

                });

                //send email to creator of issue

                var creatorTemplate = _emailRepository.GetEmailTemplateFromRoutineName("CreateIssue");

                Service.Tools.Email.SendEmailWithAttachment("", data.Email, creatorTemplate.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                       creatorTemplate.Body.Replace("{note}", model.IssueWebId == 0 ? "A new issue has been created" : "An issue has been updated")
                           .Replace("{sender}", data.Email)
                           .Replace("{object}", data.Site.Name)
                           .Replace("{tenant}", data.Tenant.Name)
                           .Replace("{type1}", data.IssueType1.Name)
                           .Replace("{phone}", data.Phone)
                           .Replace("{contactname}", data.ContactName)
                           .Replace("{address}", data.Address)
                           .Replace("{email}", data.Email)
                           .Replace("{apartment}", data.ApartmentNumber.ToString())
                           .Replace("{type}", data.IssueType2.Name)
                           .Replace("{description}", data.Description)
                           .Replace("{ispolice}", data.IsPolice.ToString())
                           .Replace("{appurl}", AppUrl)
                           .Replace("{IssueWebId}", data.IssueWebId.ToString()),
                       "", data.Image, null, true);

                if (!string.IsNullOrEmpty(data.Phone))
                {
                    var smsService = new SMS();
                    smsService.SendSMS("SER4 Application", data.Phone, creatorTemplate.SMSBody
                        .Replace("{appurl}", AppUrl)
                        .Replace("{IssueWebId}", data.IssueWebId.ToString())).Wait();
                }
            }            
        }

        public void SaveIssueWebLog(IssueWebHistoryModel model)
        {


            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = new DateTime(model.Date.year,model.Date.month,model.Date.day,model.Time.hour,model.Time.minute,model.Time.second),
                //Description = model.Description,
                DescriptionExternal = model.DescriptionExternal,
                DescriptionInternal = model.DescriptionInternal,
                ReactedBy = model.ReactedBy,
                Minutes = model.Minutes,
                MaterialCost = model.MaterialCost,
                IsWarranty = model.IsWarranty,
                IsRent = model.IsRent,
                IsDamage = model.IsDamage,
                IssueWebHistoryType = "info",
                IssueWebId = model.IssueWebId
            });

            if (!model.IsPrivate)
            {
                string AppUrl = ConfigurationManager.AppSettings["appurl"];
                var routineName = "AddIssueAction";

                var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

                var data = _repository.Get(model.IssueWebId);

                Service.Tools.Email.SendEmailWithAttachment("", data.Email, template.Subject.Replace("{IssueWebId}", data.IssueWebId.ToString()),
                template.Body
                .Replace("{appurl}", AppUrl)
                .Replace("{IssueWebId}", data.IssueWebId.ToString()),
                "", data.Image, null, true);
            }
        }

        public void sendReminder(IssueWebModel model)
        {
            var issue = _repository.Get(model.IssueWebId);
           

            var recipientType = issue.ReminderEmailSentCount >= 3 ? 6 : 1;
            var recipients = _recipientRepository.GetAllRecipients(model.SiteId).Where(x => x.IssueRecipientTypeId == recipientType).ToList();
            string AppUrl = ConfigurationManager.AppSettings["appurl"];

            recipients.ForEach(recipient =>
            {

                switch (recipientType)
                {
                    //reminder sent to technician
                    case 1:
                        if (!string.IsNullOrEmpty(recipient.Email))
                        {
                            var routineName = "ReminderEmail";

                            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

                            Service.Tools.Email.SendEmailWithAttachment("", recipient.Email, template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            template.Body.Replace("{name}", recipient.Name)
                                .Replace("{object}", model.Site.Name)
                                .Replace("{tenant}", model.Tenant.Name)
                                .Replace("{type1}", model.IssueType1.Name)
                                .Replace("{phone}", model.Phone)
                                .Replace("{contactname}", model.ContactName)
                                .Replace("{address}", model.Address)
                                .Replace("{email}", model.Email)
                                .Replace("{apartment}", model.ApartmentNumber.ToString())
                                .Replace("{type}", model.IssueType2.Name)
                                .Replace("{description}", model.Description)
                                .Replace("{ispolice}", model.IsPolice.ToString())
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            "", model.Image, null, true);
                        }
                        break;

                    //reminder sent to supervisor
                    case 6:
                        if (!string.IsNullOrEmpty(recipient.Email))
                        {
                            var routineName = "ReminderEmailLimitExceed";

                            var template = _emailRepository.GetEmailTemplateFromRoutineName(routineName);

                            Service.Tools.Email.SendEmailWithAttachment("", recipient.Email, template.Subject.Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            template.Body.Replace("{name}", recipient.Name)
                                .Replace("{object}", model.Site.Name)
                                .Replace("{tenant}", model.Tenant.Name)
                                .Replace("{type1}", model.IssueType1.Name)
                                .Replace("{phone}", model.Phone)
                                .Replace("{contactname}", model.ContactName)
                                .Replace("{address}", model.Address)
                                .Replace("{email}", model.Email)
                                .Replace("{apartment}", model.ApartmentNumber.ToString())
                                .Replace("{type}", model.IssueType2.Name)
                                .Replace("{description}", model.Description)
                                .Replace("{ispolice}", model.IsPolice.ToString())
                                .Replace("{appurl}", AppUrl)
                                .Replace("{IssueWebId}", model.IssueWebId.ToString()),
                            "", model.Image, null, true);
                        }
                        break;
                }


               
            });

            issue.ReminderEmailSentCount = issue.ReminderEmailSentCount + 1;
            _repository.Update(issue);

        
            _repository.AddIssueWebHistory(new IssueWebHistory()
            {
                Date = DateTime.Now,
                DescriptionExternal = recipientType == 1? "A reminder has been sent by the issuer to the technician": "A reminder has been sent by the issuer to the supervisor",
                DescriptionInternal = recipientType == 1 ? "A reminder has been sent by the issuer to the technician" : "A reminder has been sent by the issuer to the supervisor",
                ReactedBy = model.ContactName + " ("+model.Email+")",
                IssueWebId = model.IssueWebId,
                IssueWebHistoryType = "warning"
            });
        }


    }
}
