﻿using SERApp.API.Extensions;
using SERApp.API.Models.ResponseModels;
using SERApp.Models;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SERApp.API.Controllers
{
    public class IncidentTypeController : ApiController
    {
        private IncidentTypeService _service;
        public IncidentTypeController()
        {
            _service = new IncidentTypeService();
        }

        [HttpGet]
        [Route("IncidentType/Get")]
        public IHttpActionResult Get(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidentType(id);
                return new ResponseDataModel<IncidentTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident Type",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IncidentType/GetAll")]
        public IHttpActionResult GetAll()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetIncidentTypes();
                return new ResponseDataModel<IEnumerable<IncidentTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Loaded Incident Types",
                    Data = data
                };
            }));
        }

        [HttpGet]
        [Route("IncidentType/GetReportTypes")]
        public IHttpActionResult GetReportTypes()
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                var data = _service.GetReportTypes();
                return new ResponseDataModel<IEnumerable<ReportTypeModel>>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Report Types",
                    Data = data
                };
            }));
        }

        [HttpPost]
        [Route("IncidentType/Save")]
        public IHttpActionResult Save(IncidentTypeModel model)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.AddIncidentType(model);
                return new ResponseDataModel<IncidentTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Successfully Saved Incident Type",
                    Data = model
                };
            }));
        }

        [HttpPost]
        [Route("IncidentType/Delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.ConsistentApiHandling(() =>
            {
                _service.DeleteIncidentTypeById(id);
                return new ResponseDataModel<IncidentTypeModel>
                {
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Deleted Incident Type",
                };
            }));
        }
    }
}
