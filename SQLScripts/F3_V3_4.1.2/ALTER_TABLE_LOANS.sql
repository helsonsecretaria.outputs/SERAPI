ALTER Table Loans
ADD FirstName varchar(250);

ALTER Table Loans
ADD Email varchar(250);

ALTER Table Loans
ADD Mobile varchar(250);

ALTER Table Tenants
ADD City varchar(250);