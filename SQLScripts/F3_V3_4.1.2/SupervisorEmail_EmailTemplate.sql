
declare @IssueModuleId int;
SELECT @IssueModuleId = Id FROM Modules WHERE ShortName = 'issues';

INSERT INTO EmailTemplates(RoutineName,Description,Subject,Body,SMSBody,IsSystem,ModuleId)
VALUES ('SupervisorEmail','Supervisor Email','Issue # {IssueWebId}',
'Hi, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email. <br/><br/>Thanks,<br/>SER4 Application ',
'NotSet','false',@IssueModuleId);


declare @SupervisorEmail int;

SELECT @SupervisorEmail = Id FROM EmailTemplates WHERE RoutineName ='SupervisorEmail';


INSERT INTO EmailTemplatesTranslations(EmailTemplateId,LanguageId,Subject,Body,SMSBody)
VALUES (@SupervisorEmail,1,
'Issue # {IssueWebId}',
'Hi, <br/><br/> {note} <br/><br/>Here are the details: <br/><br/> Object: {object}<br/><br/>Tenant: {tenant}<br/><br/>Type 1: {type1}<br/><br/>Phone: {phone}<br/><br/>ContactName: {contactname}<br/><br/>Address: {address}<br/><br/>Email:{email}<br/><br/>Apartment Number: {apartment}<br/><br/> Typ: {type}<br/><br/>Description: {description}<br/><br/>Is Police: {ispolice} <br/><br/> and an image is also attached to this email. <br/><br/>Thanks,<br/>SER4 Application ',
'SMS Body (Not Set)');

