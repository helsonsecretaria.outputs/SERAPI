IF OBJECT_ID ('ReportTypes') IS NOT NULL
   DROP TABLE [dbo].[ReportTypes]
USE [SERAppDB]
GO
/****** Object:  Table [dbo].[ReportTypes]    Script Date: 4/18/2018 8:45:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](250) NULL,
 CONSTRAINT [PK_ReportTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[ReportTypes] ([Id], [Name]) VALUES (1, N'Handelserapport')
INSERT [dbo].[ReportTypes] ([Id], [Name]) VALUES (2, N'Ryckrapport')
INSERT [dbo].[ReportTypes] ([Id], [Name]) VALUES (3, N'Oppettidsrapport')
INSERT [dbo].[ReportTypes] ([Id], [Name]) VALUES (4, N'Skadegorelse/Klotterrapport')
