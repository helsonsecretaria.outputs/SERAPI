USE [SER4]
GO

DELETE FROM Type1_Fields WHERE [FieldId] = 34
DELETE FROM DailyReportFields WHERE [Id] = 34
GO

UPDATE DailyReportFields SET CustomEditorName = 'TYPE4', FieldTypeId = 10 WHERE Id = 29
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Type3TenantTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type3Id] [int] NOT NULL,
	[TenantTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Type3TenantTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Type3TenantTypes]  WITH CHECK ADD  CONSTRAINT [FK_Type3TenantTypes_TenantTypes] FOREIGN KEY([TenantTypeId])
REFERENCES [dbo].[TenantTypes] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Type3TenantTypes] CHECK CONSTRAINT [FK_Type3TenantTypes_TenantTypes]
GO

ALTER TABLE [dbo].[Type3TenantTypes]  WITH CHECK ADD  CONSTRAINT [FK_Type3TenantTypes_Type3] FOREIGN KEY([Type3Id])
REFERENCES [dbo].[Type3] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Type3TenantTypes] CHECK CONSTRAINT [FK_Type3TenantTypes_Type3]
GO

SET IDENTITY_INSERT [dbo].[DailyReportFields] ON 
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (39, 1, N'Additional Text', NULL, NULL, 0, N'OPTIONAL', NULL, 14)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (40, 1, N'Police Report', NULL, NULL, 0, N'OPTIONAL', NULL, 15)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (41, 1, N'By', NULL, NULL, 0, N'OPTIONAL', NULL, 16)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (42, 3, N'Guard', NULL, NULL, 0, N'OPTIONAL', NULL, 17)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (43, 9, N'Order Number', NULL, NULL, 0, N'OPTIONAL', NULL, 18)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (44, 1, N'Section', NULL, NULL, 0, N'OPTIONAL', NULL, 19)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (45, 2, N'Address', NULL, NULL, 0, N'OPTIONAL', NULL, 21)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (46, 6, N'Closed From', NULL, NULL, 0, N'OPTIONAL', NULL, 22)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (47, 6, N'Closed To', NULL, NULL, 0, N'OPTIONAL', NULL, 23)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (48, 2, N'Reason', NULL, NULL, 0, N'OPTIONAL', NULL, 24)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (49, 10, N'Image', NULL, NULL, 0, N'OPTIONAL', NULL, 25)
GO
SET IDENTITY_INSERT [dbo].[DailyReportFields] OFF
GO