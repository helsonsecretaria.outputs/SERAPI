DELETE FROM [dbo].[Setting] WHERE ConfigSettingId = 18
GO

DECLARE @cursor CURSOR
DECLARE @accountId int

BEGIN
	SET @cursor = CURSOR FOR SELECT [Id] FROM [dbo].[Accounts]
	
	OPEN @cursor 
	FETCH NEXT FROM @cursor INTO @accountId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT [dbo].[Setting] ([ConfigSettingId], [AccountId], [Name], [Value], [SiteId]) VALUES (18, @accountId, N'Type4', N'GM Type', 0)
		
		FETCH NEXT FROM @cursor INTO @accountId 
	END

	CLOSE @cursor;
    DEALLOCATE @cursor;
END
GO

ALTER TABLE [dbo].[DailyReportFields]
ADD
ControlGroup VARCHAR(500) NULL
GO

UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 25
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 26
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 27
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 28
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 29
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 30
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 31
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 32
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 33
GO
UPDATE DailyReportFields SET ControlGroup = 'WHITE' WHERE Id = 35
GO
UPDATE DailyReportFields SET ControlGroup = 'BLUE' WHERE Id = 36
GO
UPDATE DailyReportFields SET ControlGroup = 'BLUE' WHERE Id = 37
GO
UPDATE DailyReportFields SET ControlGroup = 'BLUE' WHERE Id = 38
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 39
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 40
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 41
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 42
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 43
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 44
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 45
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 46
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 47
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 48
GO
UPDATE DailyReportFields SET ControlGroup = 'YELLOW' WHERE Id = 49
GO