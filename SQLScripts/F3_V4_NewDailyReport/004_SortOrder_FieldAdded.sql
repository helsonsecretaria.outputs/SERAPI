USE [SER4]
GO
ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [FK_Type1_Fields_DailyReportFields]
GO
ALTER TABLE [dbo].[DailyReportFields] DROP CONSTRAINT [FK_DailyReportFields_FieldType]
GO
ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [DF_Type1_Fields_SortOrder]
GO
ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [DF_Type1_Fields_IsRequired]
GO
ALTER TABLE [dbo].[DailyReportFields] DROP CONSTRAINT [DF_DailyReportFields_SortOrder]
GO
ALTER TABLE [dbo].[DailyReportFields] DROP CONSTRAINT [DF_DailyReportFields_Type]
GO
ALTER TABLE [dbo].[DailyReportFields] DROP CONSTRAINT [DF_DailyReportFields_Required]
GO
/****** Object:  Table [dbo].[Type1_Fields]    Script Date: 1/7/2019 11:31:01 AM ******/
DROP TABLE [dbo].[Type1_Fields]
GO
/****** Object:  Table [dbo].[DailyReportFields]    Script Date: 1/7/2019 11:31:01 AM ******/
DROP TABLE [dbo].[DailyReportFields]
GO
/****** Object:  Table [dbo].[DailyReportFields]    Script Date: 1/7/2019 11:31:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyReportFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldTypeId] [int] NOT NULL,
	[DefaultLabel] [varchar](500) NULL,
	[DefaultDataSource] [varchar](max) NULL,
	[DefaultValue] [varchar](max) NULL,
	[DefaultIsRequired] [bit] NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[CustomEditorName] [varchar](50) NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_DailyReportFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type1_Fields]    Script Date: 1/7/2019 11:31:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type1_Fields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type1Id] [int] NULL,
	[FieldId] [int] NULL,
	[Label] [varchar](500) NOT NULL,
	[DataSource] [varchar](max) NULL,
	[Value] [varchar](max) NULL,
	[IsRequired] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Type1_Fields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DailyReportFields] ON 

INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (25, 10, N'Var', NULL, NULL, 1, N'BASIC', N'VAR', 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (26, 1, N'Plan', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (27, 4, N'Date', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (28, 5, N'Time', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (29, 1, N'GMType', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (30, 9, N'Number Of Persons Envolve', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (31, 9, N'Polis väntetid / Arbetstid', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (32, 10, N'Guards', NULL, NULL, 1, N'BASIC', N'GUARDS', 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (33, 10, N'Vem', NULL, NULL, 1, N'BASIC', N'VEM', 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (34, 1, N'Contact', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (35, 2, N'Description', NULL, NULL, 1, N'BASIC', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (36, 3, N'med o utan blåljus', N'["utan", "med"]', NULL, 0, N'OPTIONAL', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (37, 3, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, N'OPTIONAL', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (38, 3, N'option med 1-20', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]', NULL, 0, N'OPTIONAL', NULL, 0)
SET IDENTITY_INSERT [dbo].[DailyReportFields] OFF
SET IDENTITY_INSERT [dbo].[Type1_Fields] ON 

INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (162, 72, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (163, 72, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (164, 72, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (165, 72, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (166, 72, 29, N'GMType', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (167, 72, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (168, 72, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (169, 72, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (170, 72, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (171, 72, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (172, 72, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (173, 72, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (174, 72, 37, N'options utan 1-11', N'["2","8","9"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (175, 72, 38, N'option med 1-20', N'["15","16","17"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (176, 73, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (177, 73, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (178, 73, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (179, 73, 28, N'Time', N'null', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (180, 73, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (181, 73, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (182, 73, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (183, 73, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (184, 73, 35, N'Description', N'null', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (185, 73, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (186, 73, 38, N'option med 1-20', N'["8","18"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (187, 74, 25, N'Var', N'null', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (188, 74, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (189, 74, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (190, 74, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (191, 74, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (192, 74, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (193, 74, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (194, 74, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (195, 74, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (196, 74, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (197, 74, 37, N'options utan 1-11', N'["6"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (198, 75, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (199, 75, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (200, 75, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (201, 75, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (202, 75, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (203, 75, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (204, 75, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (205, 75, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (206, 75, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (207, 75, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (208, 75, 37, N'options utan 1-11', N'["5"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (209, 75, 38, N'option med 1-20', N'["2"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (210, 76, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (211, 76, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (212, 76, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (213, 76, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (214, 76, 29, N'GMType', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (215, 76, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (216, 76, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (217, 76, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (218, 76, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (219, 76, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (220, 76, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (221, 76, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (222, 76, 38, N'option med 1-20', N'["3","6","7","8","9","10","13","15","16","17","19","20"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (223, 77, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (224, 77, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (225, 77, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (226, 77, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (227, 77, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (228, 77, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (229, 78, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (230, 78, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (231, 78, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (232, 78, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (233, 78, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (234, 78, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (235, 78, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (236, 79, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (237, 79, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (238, 79, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (239, 79, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (240, 79, 29, N'GMType', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (241, 79, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (242, 79, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (243, 79, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (244, 79, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (245, 79, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (246, 79, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (247, 79, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (248, 79, 37, N'options utan 1-11', N'["2"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (249, 79, 38, N'option med 1-20', N'["15"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (250, 80, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (251, 80, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (252, 80, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (253, 80, 28, N'Time', N'null', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (254, 80, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (255, 80, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (256, 80, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (257, 80, 35, N'Description', N'null', NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (258, 80, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (259, 80, 37, N'options utan 1-11', N'["5"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (260, 81, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (261, 81, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (262, 81, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (263, 81, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (264, 81, 29, N'GMType', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (265, 81, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (266, 81, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (267, 81, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (268, 81, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (269, 81, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (270, 81, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (271, 81, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (272, 81, 37, N'options utan 1-11', N'["2","10"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (273, 81, 38, N'option med 1-20', N'["6"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (276, 70, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (277, 70, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (278, 70, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (279, 70, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (280, 70, 29, N'GMType', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (281, 70, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (282, 70, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (283, 70, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (284, 70, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (285, 70, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (286, 70, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (287, 70, 36, N'med o utan blåljus', N'["utan", "med"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (288, 70, 37, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (289, 70, 38, N'option med 1-20', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (290, 71, 25, N'Var', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (291, 71, 26, N'Plan', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (292, 71, 27, N'Date', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (293, 71, 28, N'Time', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (294, 71, 29, N'GMType', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (295, 71, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (296, 71, 32, N'Guards', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (297, 71, 33, N'Vem', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (298, 71, 34, N'Contact', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (299, 71, 35, N'Description', NULL, NULL, 1, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (300, 71, 36, N'med o utan blåljus', N'["utan", "med"]', NULL, 0, 0)
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (301, 71, 37, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, 0)
SET IDENTITY_INSERT [dbo].[Type1_Fields] OFF
ALTER TABLE [dbo].[DailyReportFields] ADD  CONSTRAINT [DF_DailyReportFields_Required]  DEFAULT ((0)) FOR [DefaultIsRequired]
GO
ALTER TABLE [dbo].[DailyReportFields] ADD  CONSTRAINT [DF_DailyReportFields_Type]  DEFAULT ('BASIC') FOR [Type]
GO
ALTER TABLE [dbo].[DailyReportFields] ADD  CONSTRAINT [DF_DailyReportFields_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[Type1_Fields] ADD  CONSTRAINT [DF_Type1_Fields_IsRequired]  DEFAULT ((0)) FOR [IsRequired]
GO
ALTER TABLE [dbo].[Type1_Fields] ADD  CONSTRAINT [DF_Type1_Fields_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[DailyReportFields]  WITH CHECK ADD  CONSTRAINT [FK_DailyReportFields_FieldType] FOREIGN KEY([FieldTypeId])
REFERENCES [dbo].[FieldType] ([PkFieldTypeId])
GO
ALTER TABLE [dbo].[DailyReportFields] CHECK CONSTRAINT [FK_DailyReportFields_FieldType]
GO
ALTER TABLE [dbo].[Type1_Fields]  WITH CHECK ADD  CONSTRAINT [FK_Type1_Fields_DailyReportFields] FOREIGN KEY([FieldId])
REFERENCES [dbo].[DailyReportFields] ([Id])
GO
ALTER TABLE [dbo].[Type1_Fields] CHECK CONSTRAINT [FK_Type1_Fields_DailyReportFields]
GO
