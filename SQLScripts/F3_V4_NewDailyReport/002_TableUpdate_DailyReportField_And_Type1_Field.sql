USE [SER4]
-- GO
-- ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [FK_Type1_Fields_DailyReportFields]
-- GO
-- ALTER TABLE [dbo].[DailyReportFields] DROP CONSTRAINT [FK_DailyReportFields_FieldType]
GO
ALTER TABLE [dbo].[Type1_Fields] DROP CONSTRAINT [DF_Type1_Fields_IsRequired]
GO
ALTER TABLE [dbo].[DailyReportFields] DROP CONSTRAINT [DF_DailyReportFields_Type]
GO
ALTER TABLE [dbo].[DailyReportFields] DROP CONSTRAINT [DF_DailyReportFields_Required]
GO
/****** Object:  Table [dbo].[Type1_Fields]    Script Date: 1/4/2019 2:07:11 PM ******/
DROP TABLE [dbo].[Type1_Fields]
GO
/****** Object:  Table [dbo].[DailyReportFields]    Script Date: 1/4/2019 2:07:11 PM ******/
DROP TABLE [dbo].[DailyReportFields]
GO
/****** Object:  Table [dbo].[DailyReportFields]    Script Date: 1/4/2019 2:07:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyReportFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldTypeId] [int] NOT NULL,
	[DefaultLabel] [varchar](500) NULL,
	[DefaultDataSource] [varchar](max) NULL,
	[DefaultValue] [varchar](max) NULL,
	[DefaultIsRequired] [bit] NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[CustomEditorName] [varchar](50) NULL,
 CONSTRAINT [PK_DailyReportFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type1_Fields]    Script Date: 1/4/2019 2:07:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type1_Fields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type1Id] [int] NULL,
	[FieldId] [int] NULL,
	[Label] [varchar](500) NOT NULL,
	[DataSource] [varchar](max) NULL,
	[Value] [varchar](max) NULL,
	[IsRequired] [bit] NOT NULL,
 CONSTRAINT [PK_Type1_Fields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DailyReportFields] ON 
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (25, 10, N'Var', NULL, NULL, 1, N'BASIC', N'VAR')
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (26, 1, N'Plan', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (27, 4, N'Date', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (28, 5, N'Time', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (29, 1, N'GMType', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (30, 9, N'Number Of Persons Envolve', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (31, 9, N'Polis väntetid / Arbetstid', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (32, 10, N'Guards', NULL, NULL, 1, N'BASIC', N'GUARDS')
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (33, 10, N'Vem', NULL, NULL, 1, N'BASIC', N'VEM')
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (34, 1, N'Contact', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (35, 2, N'Description', NULL, NULL, 1, N'BASIC', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (36, 3, N'med o utan blåljus', N'["utan", "med"]', NULL, 0, N'OPTIONAL', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (37, 3, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, N'OPTIONAL', NULL)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName]) VALUES (38, 3, N'option med 1-20', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]', NULL, 0, N'OPTIONAL', NULL)
GO
SET IDENTITY_INSERT [dbo].[DailyReportFields] OFF
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] ON 
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (137, 69, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 1)
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] OFF
GO
ALTER TABLE [dbo].[DailyReportFields] ADD  CONSTRAINT [DF_DailyReportFields_Required]  DEFAULT ((0)) FOR [DefaultIsRequired]
GO
ALTER TABLE [dbo].[DailyReportFields] ADD  CONSTRAINT [DF_DailyReportFields_Type]  DEFAULT ('BASIC') FOR [Type]
GO
ALTER TABLE [dbo].[Type1_Fields] ADD  CONSTRAINT [DF_Type1_Fields_IsRequired]  DEFAULT ((0)) FOR [IsRequired]
GO
ALTER TABLE [dbo].[DailyReportFields]  WITH CHECK ADD  CONSTRAINT [FK_DailyReportFields_FieldType] FOREIGN KEY([FieldTypeId])
REFERENCES [dbo].[FieldType] ([PkFieldTypeId])
GO
ALTER TABLE [dbo].[DailyReportFields] CHECK CONSTRAINT [FK_DailyReportFields_FieldType]
GO
ALTER TABLE [dbo].[Type1_Fields]  WITH CHECK ADD  CONSTRAINT [FK_Type1_Fields_DailyReportFields] FOREIGN KEY([FieldId])
REFERENCES [dbo].[DailyReportFields] ([Id])
GO
ALTER TABLE [dbo].[Type1_Fields] CHECK CONSTRAINT [FK_Type1_Fields_DailyReportFields]
GO
