USE [SER4]
GO
SET IDENTITY_INSERT [dbo].[ConfigSettings] ON 
GO
INSERT [dbo].[ConfigSettings] ([Id], [SettingName], [SettingLabel], [DefaultValue], [ModuleId], [InputType]) VALUES (18, N'Type4', N'Type4', N'Type4', 6, N'text')
GO
SET IDENTITY_INSERT [dbo].[ConfigSettings] OFF
GO