USE SER4
GO
UPDATE [DailyReportFields] SET [CustomEditorName] = 'WITHOUTBLUELIGHTOPTIONS' WHERE Id = 37
UPDATE [DailyReportFields] SET [CustomEditorName] = 'WITHBLUELIGHTOPTIONS' WHERE Id = 38
UPDATE [DailyReportFields] SET [CustomEditorName] = 'IMAGEPICKER' WHERE Id = 49
UPDATE [DailyReportFields] SET [CustomEditorName] = 'GUARDS', FieldTypeId = 10 WHERE Id = 42
UPDATE [DailyReportFields] SET FieldTypeId = 2 WHERE Id = 39
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] ON 
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (162, 72, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (163, 72, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (164, 72, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (165, 72, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (166, 72, 29, N'GMType', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (167, 72, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (168, 72, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (169, 72, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (170, 72, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (172, 72, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (173, 72, 36, N'med o utan bl�ljus', N'["utan","med"]', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (174, 72, 37, N'options utan 1-11', N'["2","8","9"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (175, 72, 38, N'option med 1-20', N'["15","16","17"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (176, 73, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (177, 73, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (178, 73, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (179, 73, 28, N'Time', N'null', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (180, 73, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (181, 73, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (182, 73, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (184, 73, 35, N'Description', N'null', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (185, 73, 36, N'med o utan bl�ljus', N'["utan","med"]', N'med', 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (186, 73, 38, N'option med 1-20', N'["8","18"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (187, 74, 25, N'Var', N'null', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (188, 74, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (189, 74, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (190, 74, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (191, 74, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (192, 74, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (193, 74, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (195, 74, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (196, 74, 36, N'med o utan bl�ljus', N'["utan","med"]', N'utan', 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (197, 74, 37, N'options utan 1-11', N'["6"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (198, 75, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (199, 75, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (200, 75, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (201, 75, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (202, 75, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (203, 75, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (204, 75, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (206, 75, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (207, 75, 36, N'med o utan bl�ljus', N'["utan","med"]', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (208, 75, 37, N'options utan 1-11', N'["5"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (209, 75, 38, N'option med 1-20', N'["2"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (210, 76, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (211, 76, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (212, 76, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (213, 76, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (214, 76, 29, N'GMType', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (215, 76, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (216, 76, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (217, 76, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (218, 76, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (220, 76, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (221, 76, 36, N'med o utan bl�ljus', N'["utan","med"]', N'med', 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (222, 76, 38, N'option med 1-20', N'["3","6","7","8","9","10","13","15","16","17","19","20"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (223, 77, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (224, 77, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (225, 77, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (226, 77, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (227, 77, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (228, 77, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (229, 78, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (230, 78, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (231, 78, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (232, 78, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (233, 78, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (235, 78, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (236, 79, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (237, 79, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (238, 79, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (239, 79, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (240, 79, 29, N'GMType', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (241, 79, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (242, 79, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (243, 79, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (244, 79, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (246, 79, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (247, 79, 36, N'med o utan bl�ljus', N'["utan","med"]', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (248, 79, 37, N'options utan 1-11', N'["2"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (249, 79, 38, N'option med 1-20', N'["15"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (250, 80, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (251, 80, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (252, 80, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (253, 80, 28, N'Time', N'null', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (254, 80, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (255, 80, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (257, 80, 35, N'Description', N'null', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (258, 80, 36, N'med o utan bl�ljus', N'["utan","med"]', N'utan', 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (259, 80, 37, N'options utan 1-11', N'["5"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (260, 81, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (261, 81, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (262, 81, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (263, 81, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (264, 81, 29, N'GMType', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (265, 81, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (266, 81, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (267, 81, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (268, 81, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (270, 81, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (271, 81, 36, N'med o utan bl�ljus', N'["utan","med"]', N'med', 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (272, 81, 37, N'options utan 1-11', N'["2","10"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (273, 81, 38, N'option med 1-20', N'["6"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (290, 71, 25, N'Var', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (291, 71, 26, N'Plan', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (292, 71, 27, N'Date', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (293, 71, 28, N'Time', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (294, 71, 29, N'GMType', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (295, 71, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (296, 71, 32, N'Guards', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (297, 71, 33, N'Vem', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (299, 71, 35, N'Description', NULL, NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (300, 71, 36, N'med o utan bl�ljus', N'["utan", "med"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (301, 71, 37, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (358, 70, 25, N'Var', N'null', NULL, 1, 0, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (359, 70, 26, N'Plan', N'null', NULL, 1, 1, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (360, 70, 27, N'Date', N'null', NULL, 1, 2, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (361, 70, 28, N'Time', NULL, NULL, 1, 3, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (362, 70, 29, N'GMType', NULL, NULL, 1, 4, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (363, 70, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 5, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (364, 70, 31, N'Polis v�ntetid / Arbetstid', NULL, NULL, 1, 6, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (365, 70, 32, N'Guards', NULL, NULL, 1, 7, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (366, 70, 33, N'Vem', NULL, NULL, 1, 8, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (368, 70, 35, N'Description', NULL, NULL, 1, 10, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (369, 70, 36, N'med o utan bl�ljus', N'["utan","med"]', N'utan', 0, 11, NULL)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder], [ExcludedValues]) VALUES (370, 70, 37, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, 12, NULL)
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] OFF
GO