USE [SER4]

DELETE FROM [Type1_Fields]
DELETE FROM [Type1]

GO
SET IDENTITY_INSERT [dbo].[Type1] ON 
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (1, N'Test', 1, N'1,2,3,4,5', N'1,2,3,4,5', 36, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (2, N'Avvisning', 1, N'0', N'1,2,3,4,5', 59, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (3, N'Avlägsning', 1, N'0', N'0', 59, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (4, N'Bedrägeri/försök', 1, N'0', N'0', 59, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (5, N'Bombhot', 1, N'0', N'0', 59, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (6, N'Klotter', 1, N'1,2,3,4,5', N'1,2,3,4,5', 59, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (7, N'Brand', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (8, N'Ficktjuvar', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (9, N'Hot & Våld', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (10, N'Iakttagelse / Oro', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (11, N'Inbrott & Försök', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (12, N'Klotter', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (13, N'Ryck BRF', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (14, N'Ryck JSAB', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (15, N'Rån & Försök', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (16, N'Service/Stöttning', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (17, N'Sjukdom/Olycka', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (18, N'Skadegörelse', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (19, N'SSA', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (20, N'Stöld & Snatteri', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (21, N'Övrigt', 1, N'1,2,3,4,5', N'1,2,3,4,5', 69, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (23, N'Brand', 1, N'1,2,3,4,5', N'1,2,3,4,5', 68, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (26, N'Type1', 1, N'1,2', N'1,2', 73, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (27, N'A', 1, N'0', N'0', 74, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (28, N'Hot', 1, N'0', N'0', 74, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (29, N'Brand2', 1, N'0', N'0', 74, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (30, N'Bandlarm', 1, N'1', N'1', 79, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (31, N'Hot & Våld1', 1, N'1', N'1', 79, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (32, N'Rondering', 1, N'0', N'0', 79, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (33, N'Ronderingar', 1, N'1,6,2,3,4,5', N'1,6,2,3,4,5', 68, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (34, N'Brand.', 1, N'1,2,3,4,5', N'1,2,3,4,5', 86, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (36, N'Brand1', 1, N'1,2,3,4,5', N'1,2,3,4,5', 88, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (37, N'Brand', 1, N'1,2,3,4,5', N'1,2,3,4,5', 89, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (38, N'type1', 1, N'2', N'2', 93, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (39, N'Ryck JSAB', 1, N'2', N'2', 68, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (40, N'a', 1, N'0', N'0', 96, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (41, N'external', 1, N'6', N'6', 98, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (42, N'Assistance', 1, N'0', N'0', 100, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (43, N'Alarm', 1, N'6', N'6', 100, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (44, N'Other incidents', 1, N'0', N'0', 100, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (45, N'Brand', 1, N'0', N'2', 103, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (46, N'Inbrott', 1, N'2', N'2', 103, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (70, N'Avlägsning', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (71, N'Avvisning', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (72, N'Bedrägeri/försök', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (73, N'Bombhot', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (74, N'Borttappat barn', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (75, N'Brand/brandlarm', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (76, N'Envarsgrip', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (77, N'Event', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (78, N'Felanmälan', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (79, N'Ficktjuveri', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (80, N'Hisslarm', 1, N'0', N'0', 70, 0)
GO
INSERT [dbo].[Type1] ([Id], [Name], [IsReportRequired], [RequiredReport], [ShownReport], [AccountId], [SiteId]) VALUES (81, N'Hot o våld', 1, N'0', N'0', 70, 0)
GO
SET IDENTITY_INSERT [dbo].[Type1] OFF
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] ON 
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (138, 70, 25, N'Var', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (139, 70, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (140, 70, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (141, 70, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (142, 70, 29, N'GMType', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (143, 70, 30, N'Number Of Persons Envolve', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (144, 70, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (145, 70, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (146, 70, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (147, 70, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (148, 70, 36, N'med o utan blåljus', N'["utan", "med"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (149, 70, 37, N'options utan 1-11', N'["1","10"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (150, 71, 25, N'Var', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (151, 71, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (152, 71, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (153, 71, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (154, 71, 29, N'GMType', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (155, 71, 30, N'Number Of Persons Envolve', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (156, 71, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (157, 71, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (158, 71, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (159, 71, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (160, 71, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (161, 71, 37, N'options utan 1-11', N'["1","10"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (162, 72, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (163, 72, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (164, 72, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (165, 72, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (166, 72, 29, N'GMType', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (167, 72, 30, N'Number Of Persons Envolve', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (168, 72, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (169, 72, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (170, 72, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (171, 72, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (172, 72, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (173, 72, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (174, 72, 37, N'options utan 1-11', N'["2","8","9"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (175, 72, 38, N'option med 1-20', N'["15","16","17"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (176, 73, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (177, 73, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (178, 73, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (179, 73, 28, N'Time', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (180, 73, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (181, 73, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (182, 73, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (183, 73, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (184, 73, 35, N'Description', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (185, 73, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (186, 73, 38, N'option med 1-20', N'["8","18"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (187, 74, 25, N'Var', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (188, 74, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (189, 74, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (190, 74, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (191, 74, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (192, 74, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (193, 74, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (194, 74, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (195, 74, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (196, 74, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (197, 74, 37, N'options utan 1-11', N'["6"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (198, 75, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (199, 75, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (200, 75, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (201, 75, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (202, 75, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (203, 75, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (204, 75, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (205, 75, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (206, 75, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (207, 75, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (208, 75, 37, N'options utan 1-11', N'["5"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (209, 75, 38, N'option med 1-20', N'["2"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (210, 76, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (211, 76, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (212, 76, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (213, 76, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (214, 76, 29, N'GMType', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (215, 76, 30, N'Number Of Persons Envolve', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (216, 76, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (217, 76, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (218, 76, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (219, 76, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (220, 76, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (221, 76, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (222, 76, 38, N'option med 1-20', N'["3","6","7","8","9","10","13","15","16","17","19","20"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (223, 77, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (224, 77, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (225, 77, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (226, 77, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (227, 77, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (228, 77, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (229, 78, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (230, 78, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (231, 78, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (232, 78, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (233, 78, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (234, 78, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (235, 78, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (236, 79, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (237, 79, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (238, 79, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (239, 79, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (240, 79, 29, N'GMType', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (241, 79, 30, N'Number Of Persons Envolve', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (242, 79, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (243, 79, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (244, 79, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (245, 79, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (246, 79, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (247, 79, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (248, 79, 37, N'options utan 1-11', N'["2"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (249, 79, 38, N'option med 1-20', N'["15"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (250, 80, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (251, 80, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (252, 80, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (253, 80, 28, N'Time', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (254, 80, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (255, 80, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (256, 80, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (257, 80, 35, N'Description', N'null', NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (258, 80, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (259, 80, 37, N'options utan 1-11', N'["5"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (260, 81, 25, N'Var', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (261, 81, 26, N'Plan', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (262, 81, 27, N'Date', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (263, 81, 28, N'Time', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (264, 81, 29, N'GMType', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (265, 81, 30, N'Number Of Persons Envolve', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (266, 81, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (267, 81, 32, N'Guards', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (268, 81, 33, N'Vem', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (269, 81, 34, N'Contact', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (270, 81, 35, N'Description', NULL, NULL, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (271, 81, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (272, 81, 37, N'options utan 1-11', N'["2","10"]', NULL, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired]) VALUES (273, 81, 38, N'option med 1-20', N'["6"]', NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] OFF
GO
