USE [SER4]
GO
DELETE FROM [dbo].[Type1_Fields]
GO
DELETE FROM [dbo].[DailyReportFields]
GO
SET IDENTITY_INSERT [dbo].[DailyReportFields] ON 
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (25, 10, N'Var', NULL, NULL, 1, N'BASIC', N'VAR', 0)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (26, 1, N'Plan', NULL, NULL, 1, N'BASIC', NULL, 1)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (27, 4, N'Date', NULL, NULL, 1, N'BASIC', NULL, 2)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (28, 5, N'Time', NULL, NULL, 1, N'BASIC', NULL, 3)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (29, 1, N'GMType', NULL, NULL, 1, N'BASIC', NULL, 4)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (30, 9, N'Number Of Persons Envolve', NULL, NULL, 1, N'BASIC', NULL, 5)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (31, 9, N'Polis väntetid / Arbetstid', NULL, NULL, 1, N'BASIC', NULL, 6)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (32, 10, N'Guards', NULL, NULL, 1, N'BASIC', N'GUARDS', 7)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (33, 10, N'Vem', NULL, NULL, 1, N'BASIC', N'VEM', 8)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (34, 1, N'Contact', NULL, NULL, 1, N'BASIC', NULL, 9)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (35, 2, N'Description', NULL, NULL, 1, N'BASIC', NULL, 10)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (36, 3, N'med o utan blåljus', N'["utan", "med"]', NULL, 0, N'OPTIONAL', NULL, 11)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (37, 3, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, N'OPTIONAL', NULL, 12)
GO
INSERT [dbo].[DailyReportFields] ([Id], [FieldTypeId], [DefaultLabel], [DefaultDataSource], [DefaultValue], [DefaultIsRequired], [Type], [CustomEditorName], [SortOrder]) VALUES (38, 3, N'option med 1-20', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]', NULL, 0, N'OPTIONAL', NULL, 13)
GO
SET IDENTITY_INSERT [dbo].[DailyReportFields] OFF
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] ON 
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (162, 72, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (163, 72, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (164, 72, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (165, 72, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (166, 72, 29, N'GMType', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (167, 72, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (168, 72, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (169, 72, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (170, 72, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (171, 72, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (172, 72, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (173, 72, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (174, 72, 37, N'options utan 1-11', N'["2","8","9"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (175, 72, 38, N'option med 1-20', N'["15","16","17"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (176, 73, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (177, 73, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (178, 73, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (179, 73, 28, N'Time', N'null', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (180, 73, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (181, 73, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (182, 73, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (183, 73, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (184, 73, 35, N'Description', N'null', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (185, 73, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (186, 73, 38, N'option med 1-20', N'["8","18"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (187, 74, 25, N'Var', N'null', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (188, 74, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (189, 74, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (190, 74, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (191, 74, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (192, 74, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (193, 74, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (194, 74, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (195, 74, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (196, 74, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (197, 74, 37, N'options utan 1-11', N'["6"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (198, 75, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (199, 75, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (200, 75, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (201, 75, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (202, 75, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (203, 75, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (204, 75, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (205, 75, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (206, 75, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (207, 75, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (208, 75, 37, N'options utan 1-11', N'["5"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (209, 75, 38, N'option med 1-20', N'["2"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (210, 76, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (211, 76, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (212, 76, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (213, 76, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (214, 76, 29, N'GMType', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (215, 76, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (216, 76, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (217, 76, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (218, 76, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (219, 76, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (220, 76, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (221, 76, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (222, 76, 38, N'option med 1-20', N'["3","6","7","8","9","10","13","15","16","17","19","20"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (223, 77, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (224, 77, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (225, 77, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (226, 77, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (227, 77, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (228, 77, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (229, 78, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (230, 78, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (231, 78, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (232, 78, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (233, 78, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (234, 78, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (235, 78, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (236, 79, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (237, 79, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (238, 79, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (239, 79, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (240, 79, 29, N'GMType', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (241, 79, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (242, 79, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (243, 79, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (244, 79, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (245, 79, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (246, 79, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (247, 79, 36, N'med o utan blåljus', N'["utan","med"]', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (248, 79, 37, N'options utan 1-11', N'["2"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (249, 79, 38, N'option med 1-20', N'["15"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (250, 80, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (251, 80, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (252, 80, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (253, 80, 28, N'Time', N'null', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (254, 80, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (255, 80, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (256, 80, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (257, 80, 35, N'Description', N'null', NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (258, 80, 36, N'med o utan blåljus', N'["utan","med"]', N'utan', 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (259, 80, 37, N'options utan 1-11', N'["5"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (260, 81, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (261, 81, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (262, 81, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (263, 81, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (264, 81, 29, N'GMType', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (265, 81, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (266, 81, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (267, 81, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (268, 81, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (269, 81, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (270, 81, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (271, 81, 36, N'med o utan blåljus', N'["utan","med"]', N'med', 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (272, 81, 37, N'options utan 1-11', N'["2","10"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (273, 81, 38, N'option med 1-20', N'["6"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (290, 71, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (291, 71, 26, N'Plan', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (292, 71, 27, N'Date', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (293, 71, 28, N'Time', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (294, 71, 29, N'GMType', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (295, 71, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (296, 71, 32, N'Guards', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (297, 71, 33, N'Vem', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (298, 71, 34, N'Contact', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (299, 71, 35, N'Description', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (300, 71, 36, N'med o utan blåljus', N'["utan", "med"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (301, 71, 37, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (302, 70, 25, N'Var', NULL, NULL, 1, 0)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (303, 70, 26, N'Plan', NULL, NULL, 1, 1)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (304, 70, 27, N'Date', NULL, NULL, 1, 2)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (305, 70, 28, N'Time', NULL, NULL, 1, 3)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (306, 70, 29, N'GMType', NULL, NULL, 1, 4)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (307, 70, 30, N'Number Of Persons Envolve', NULL, NULL, 1, 5)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (308, 70, 31, N'Polis väntetid / Arbetstid', NULL, NULL, 1, 6)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (309, 70, 32, N'Guards', NULL, NULL, 1, 7)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (310, 70, 33, N'Vem', NULL, NULL, 1, 8)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (311, 70, 34, N'Contact', NULL, NULL, 1, 9)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (312, 70, 35, N'Description', NULL, NULL, 1, 10)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (313, 70, 36, N'med o utan blåljus', N'["utan", "med"]', NULL, 0, 11)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (314, 70, 37, N'options utan 1-11', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]', NULL, 0, 12)
GO
INSERT [dbo].[Type1_Fields] ([Id], [Type1Id], [FieldId], [Label], [DataSource], [Value], [IsRequired], [SortOrder]) VALUES (315, 70, 38, N'option med 1-20', N'["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]', NULL, 0, 13)
GO
SET IDENTITY_INSERT [dbo].[Type1_Fields] OFF
GO
