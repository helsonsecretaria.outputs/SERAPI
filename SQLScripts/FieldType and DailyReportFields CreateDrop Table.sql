USE [SER4]
GO
/****** Object:  Table [dbo].[DailyReportFields]    Script Date: 12/21/2018 9:04:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyReportFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldType] [int] NOT NULL,
	[DefaultValue] [varchar](250) NULL,
	[Label] [varchar](250) NULL,
	[Key] [int] NULL,
	[Required] [bit] NOT NULL,
 CONSTRAINT [PK_DailyReportFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FieldType]    Script Date: 12/21/2018 9:04:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FieldType](
	[PkFieldTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Flag] [varchar](250) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Description] [varchar](5000) NULL,
 CONSTRAINT [PK_FieldType] PRIMARY KEY CLUSTERED 
(
	[PkFieldTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DailyReportFields] ON 

INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (1, 4, NULL, N'Date', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (2, 5, NULL, N'Time', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (3, 9, NULL, N'Number of Person', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (4, 9, NULL, N'Police Wait Time', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (5, 10, NULL, N'Guards', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (6, 3, NULL, N'Vem', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (7, 3, NULL, N'Contact', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (8, 2, NULL, N'Description', NULL, 0)
INSERT [dbo].[DailyReportFields] ([Id], [FieldType], [DefaultValue], [Label], [Key], [Required]) VALUES (9, 3, NULL, N'Var', NULL, 0)
SET IDENTITY_INSERT [dbo].[DailyReportFields] OFF
SET IDENTITY_INSERT [dbo].[FieldType] ON 

INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (1, N'TEXT', N'Text', N'Simple Text Field')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (2, N'TEXTAREA', N'Text Area', N'Simple Text Area')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (3, N'SELECT', N'Dropdown', N'Simple Dropdown')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (4, N'DATE', N'Date', N'Simple Date Picker')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (5, N'TIME', N'Time', N'Simple Time Picker')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (6, N'DATETIME', N'Date And Time', N'Date And Time Picker')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (7, N'CHECKBOX', N'Check Box', N'Simple Checkbox')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (8, N'RADIOBUTTON', N'Radio Button', N'Simple Radio Button')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (9, N'NUMERIC', N'Number', N'Simple Numeric Field')
INSERT [dbo].[FieldType] ([PkFieldTypeId], [Flag], [Name], [Description]) VALUES (10, N'CUSTOM', N'Custom', N'Cumtom Field')
SET IDENTITY_INSERT [dbo].[FieldType] OFF
ALTER TABLE [dbo].[DailyReportFields] ADD  CONSTRAINT [DF_DailyReportFields_Required]  DEFAULT ((0)) FOR [Required]
GO
ALTER TABLE [dbo].[DailyReportFields]  WITH CHECK ADD  CONSTRAINT [FK_DailyReportFields_FieldType] FOREIGN KEY([FieldType])
REFERENCES [dbo].[FieldType] ([PkFieldTypeId])
GO
ALTER TABLE [dbo].[DailyReportFields] CHECK CONSTRAINT [FK_DailyReportFields_FieldType]
GO
