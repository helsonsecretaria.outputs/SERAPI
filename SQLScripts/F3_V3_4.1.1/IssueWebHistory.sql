USE [SER4prod]
GO
/****** Object:  Table [dbo].[IssueWebHistory]    Script Date: 9/25/2018 12:49:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IssueWebHistory](
	[IssueWebHistoryId] [int] NOT NULL,
	[ReactedBy] [varchar](250) NULL,
	[Minutes] [int] NULL,
	[MaterialCost] [int] NULL,
	[Description] [varchar](max) NULL,
	[Date] [datetime] NULL,
	[IsWarranty] [bit] NULL,
	[IsRent] [bit] NULL,
	[IsDamage] [bit] NULL,
 CONSTRAINT [PK_IssueWebHistory] PRIMARY KEY CLUSTERED 
(
	[IssueWebHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
