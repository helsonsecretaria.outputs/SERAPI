USE [SERAppDB]
GO
/****** Object:  Table [dbo].[IncidentGuard]    Script Date: 4/4/2018 8:44:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IncidentGuard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IncidentId] [int] NULL,
	[GuardId] [int] NULL,
 CONSTRAINT [PK_IncidentGuard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
