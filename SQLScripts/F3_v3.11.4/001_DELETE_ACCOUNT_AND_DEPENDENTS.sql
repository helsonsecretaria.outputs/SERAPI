DELETE FROM Loans
	WHERE SiteId IN 
		(SELECT Id FROM Sites WHERE AccountId IN
			(SELECT Id FROM Accounts WHERE IsDeleted = 1)
		)

DELETE FROM Tenants
	WHERE SiteId IN 
		(SELECT Id FROM Sites WHERE AccountId IN
			(SELECT Id FROM Accounts WHERE IsDeleted = 1)
		)

DELETE FROM UserRoles
	WHERE UserId IN
		(SELECT Id FROM Users WHERE AccountId IN
			(SELECT Id FROM Accounts WHERE IsDeleted = 1)
		)

DELETE FROM UserModuleRoles
	WHERE UserId IN
		(SELECT Id FROM Users WHERE AccountId IN
			(SELECT Id FROM Accounts WHERE IsDeleted = 1)
		)

DELETE FROM Users WHERE AccountId IN
	(SELECT Id FROM Accounts WHERE IsDeleted = 1)


DELETE FROM Sites
	WHERE AccountId IN
	(SELECT Id FROM Accounts WHERE IsDeleted = 1)

DELETE FROM AccountModules
	WHERE AccountId IN
	(SELECT Id FROM Accounts WHERE IsDeleted = 1)

DELETE FROM Accounts WHERE IsDeleted = 1

