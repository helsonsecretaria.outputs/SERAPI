﻿using SER.FileManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SER.FileManager.Repository
{
    public class UserRoleModuleRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings["SERAppDBContext"].ConnectionString;
        public UserRoleModuleRepository(){
        }

        public List<UserModuleAndRoleModel> GetModuleAndRoles(int userId)
        {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                List<UserModuleAndRoleModel> userModuleAndRoles = new List<UserModuleAndRoleModel>();
                string query = $@"SELECT DISTINCT r.Id as RoleId, r.Name as RoleName, 
                            m.Id as ModuleId, m.ShortName as ModuleName, s.Id as SiteId, 
                            s.Name as SiteName FROM UserModuleRoles ur
                            INNER JOIN Roles r ON ur.RoleId = r.Id
                            INNER JOIN Modules m ON ur.ModuleId = m.Id
                            INNER JOIN Sites s ON ur.SiteId = s.Id
                            WHERE ur.UserId = @UserId;";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@UserId", userId);  
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader.HasRows) {
                            var userModuleAndRole = new UserModuleAndRoleModel();
                            userModuleAndRole.RoleId = Convert.ToInt32(reader[0]);
                            userModuleAndRole.RoleName = reader[1].ToString();
                            userModuleAndRole.ModuleId = Convert.ToInt32(reader[2]);
                            userModuleAndRole.ModuleName = reader[3].ToString();
                            userModuleAndRole.SiteId = Convert.ToInt32(reader[4]);
                            userModuleAndRole.SiteName = reader[5].ToString();
                            userModuleAndRoles.Add(userModuleAndRole);
                        }
                    }
                    reader.Close();
                    return userModuleAndRoles;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public List<UserModuleAndRoleModel> GetModuleAndRolesForAdmin(int userId) {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                List<UserModuleAndRoleModel> userModuleAndRoles = new List<UserModuleAndRoleModel>();
                string query = $@"SELECT DISTINCT 1 as RoleId, 'admin' as RoleName, 
                            m.Id as ModuleId, m.ShortName as ModuleName, s.Id as SiteId, s.Name as SiteName FROM AccountModules am 
                            INNER JOIN Accounts a ON a.Id = am.AccountId
                            INNER JOIN Modules m ON m.Id = am.ModuleId
                            INNER JOIN Users u ON u.AccountId = a.Id
                            INNER JOIN Sites s ON s.AccountId = a.Id
                            WHERE u.Id = @UserId;";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@UserId", userId);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            var userModuleAndRole = new UserModuleAndRoleModel();
                            userModuleAndRole.RoleId = Convert.ToInt32(reader[0]);
                            userModuleAndRole.RoleName = reader[1].ToString();
                            userModuleAndRole.ModuleId = Convert.ToInt32(reader[2]);
                            userModuleAndRole.ModuleName = reader[3].ToString();
                            userModuleAndRole.SiteId = Convert.ToInt32(reader[4]);
                            userModuleAndRole.SiteName = reader[5].ToString();
                            userModuleAndRoles.Add(userModuleAndRole);
                        }
                    }
                    reader.Close();
                    return userModuleAndRoles;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public List<ModuleModel> GetModules() {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                List<ModuleModel> modules = new List<ModuleModel>();
                string query = $@"SELECT DISTINCT Id, ShortName, Title FROM Modules WHERE IsActive = 1";
                SqlCommand command = new SqlCommand(query, connection);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            var module = new ModuleModel();
                            module.Id = Convert.ToInt32(reader[0]);
                            module.ShortName = reader[1].ToString();
                            module.Title = reader[2].ToString();
                            modules.Add(module);
                        }
                    }
                    reader.Close();
                    return modules;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public Dictionary<int, string> GetRoles()
        {
            var result = new Dictionary<int, string>();

            using (SqlConnection connection = new SqlConnection(connString))
            {
                string query = $@"SELECT Id, Name FROM Roles WHERE IsFileAccess = 1";
                SqlCommand command = new SqlCommand(query, connection);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            result.Add(Convert.ToInt32(reader[0]), reader[1].ToString());
                        }
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return result;
        }
    }
}