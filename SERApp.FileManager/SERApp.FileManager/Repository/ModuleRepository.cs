﻿using SER.FileManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SER.FileManager.Repository
{
    public class ModuleRepository
    {
        private string connString = ConfigurationManager.ConnectionStrings["SERAppDBContext"].ConnectionString;

        public ModuleRepository()
        {

        }

        public ModuleModel GetModule(int id)
        {
            var result = default(ModuleModel);

            using (SqlConnection connection = new SqlConnection(connString))
            {
                var query = $"SELECT [Modules].[ShortName] FROM [Modules] WHERE Id = {id}";
                var command = new SqlCommand(query, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if (reader.HasRows)
                        {
                            result = new ModuleModel();
                            result.ShortName = reader[0].ToString();
                        }
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return result;
        }
    }
}