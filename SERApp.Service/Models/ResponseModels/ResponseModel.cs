﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Models.ResponseModels
{
    public class ResponseModel
    {
        /// <summary>
        /// The success integer code.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// The Error integer code.
        /// </summary>
        public int ErrorCode { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// The response string returned.
        /// </summary>
        public string Message { get; set; }

        public object Extras { get; set; }
    }
}
