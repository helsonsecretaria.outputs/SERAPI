﻿using SERApp.Data.Models;
using SERApp.Models;
using SERApp.Repository.Interface;
using SERApp.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public interface IIssueType3Service
    {
        IEnumerable<IssueType3Model> GetAll(int accountId);
        bool Save(IssueType3Model model);
        bool Delete(int id);
        IssueType3Model Get(int id);
    }

    public class IssueType3Service : IIssueType3Service
    {
        private readonly IRepository<IssueType3> _repository;
        private readonly IIssueWebRepository _issueWebRepository;
        private readonly IRepository<Issue_IssueType3> _repositoryIssueType3;
        public IssueType3Service()
        {
            _repository = new Repository<IssueType3>();
            _issueWebRepository = new IssueWebRepository();
            _repositoryIssueType3 = new Repository<Issue_IssueType3>();
        }

        public IssueType3Model Get(int id)
        {
            var model = new IssueType3Model();
            var data = _repository.Get(id);
            model.AccountId = data.AccountId;
            model.Id = data.Id;
            model.Name = data.Name;
            //model.Value = data.Value;

            return model;
        }

        public IEnumerable<IssueType3Model> GetAll(int accountId)
        {
            return _repository.GetAll().Where(x => x.AccountId == accountId).Select(x => new IssueType3Model()
            {
                AccountId = x.AccountId,
                Id = x.Id,
                Name = x.Name,
                //Value = x.Value
            });
        }

        public bool Save(IssueType3Model model)
        {
            try
            {
                if (model.Id == 0)
                {
                    var data = new IssueType3();
                    data.AccountId = model.AccountId;
                    data.Name = model.Name;
                    //data.Value = model.Value;

                    _repository.Save(data);
                    return true;
                }
                else
                {
                    var data = _repository.Get(model.Id);
                    data.Name = model.Name;
                    data.AccountId = model.AccountId;
                    //data.Value = model.Value;
                    _repository.Update(data);
                    return true;
                }

            }
            catch (Exception ex) { return false; }

        }
        public bool Delete(int id)
        {
            //var data = _issueWebRepository.GetByPredicate(x => x.Issue_IssueType3.Any(r => r.Id == id)).ToList();
            //var issuetype3 = _repository.Get(id);
            var data = _repositoryIssueType3.GetByPredicate(x => x.IssueType3Id == id).ToList();
            if (_repositoryIssueType3.GetByPredicate(x => x.IssueType3Id == id).ToList().Any(r => r.Value == true))
            {
                return false;
            }


            _repository.Delete(id);
            return true;
        }
    }
}
