﻿using SERApp.Models;
using SERApp.Service.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Service.Services
{
    public class SMSService
    {
        SMS sms;
        public SMSService() {
            sms = new SMS();
        }

        public async Task<SMSResponseModel> SendSMS(LoanModel model)
        {
            var smsModel = new OutgoingSMSModel()
            {
                From = "iP1",
                Numbers = new List<string>() { model.Customer.MobileNumber },
                Message = "test",
                Prio = 1,
                Email = false
            };
            string smsParams = Newtonsoft.Json.JsonConvert.SerializeObject(smsModel);
            return await sms.SendSMS(smsParams);
        }

        public async Task<SMSResponseModel> TestSendSMS(string number) {
            var smsModel = new OutgoingSMSModel()
            {
                From = "iP1",
                Numbers = new List<string>() { number },
                Message = "test",
                Prio = 1,
                Email = false
            };
            string smsParams = Newtonsoft.Json.JsonConvert.SerializeObject(smsModel);
            return await sms.SendSMS(smsParams);
        }
    }
}
