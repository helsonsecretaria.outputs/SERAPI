﻿(function (ko) {
    var componentId = "mobile-loan-create";
    //private functions
    var ViewModel = function (params) {
        var self = this;
        self.searchCustomerEnabled = ko.observable(true);
        self.customerFieldsEnabled = ko.observable(true);

        self.customerFormVisible = ko.observable(true);
        self.loanFormVisible = ko.observable(false);
        self.eventFormVisible = ko.observable(false);
        self.notificationFormVisible = ko.observable(false);

        self.facilityData = ko.observableArray([
            { name: 'Manufacturing Facility', value: '1' },
            { name: 'Industry and Service Facility', value: '2' },
            { name: 'Health Care Facility', value: '3' },
            { name: 'Public Service Facility', value: '4' },
        ]);
        //should get from db next
        self.Type = ko.observableArray([
            { name: 'Keys', value: '1' },
            { name: 'Passes', value: '2' },
            { name: 'Other', value: '3' }
        ]);

        self.toggleNavigate = function (component, command) {

            if (component == 'customer' && command == 'next') {
                self.loanFormVisible(true);
                self.animateScrollDown('#create-loan');
                $('#create-loan').focus();
                setTimeout(function () {
                    self.customerFormVisible(false);
                }, 500)
            }
            else if (component == 'loan' && command == 'back') {
                self.customerFormVisible(true);
                self.animateScrollDown('#create-customer');
                $('#create-customer').focus();
                setTimeout(function () {
                    self.loanFormVisible(false);
                })
            }
            else if (component == 'loan' && command == 'next') {
                self.eventFormVisible(true);
                self.animateScrollDown('#create-event');
                $('#create-event').focus();
                setTimeout(function () {
                    self.loanFormVisible(false);
                }, 500)
            }
            else if (component == 'event' && command == 'back') {
                self.loanFormVisible(true);
                self.animateScrollDown('#create-loan');
                $('#create-loan').focus();
                setTimeout(function () {
                    self.eventFormVisible(false);
                })
            }
            else if (component == 'event' && command == 'next') {
                self.eventFormVisible(false);
                self.animateScrollDown('#create-notification');
                $('#create-notification').focus();
                setTimeout(function () {
                    self.notificationFormVisible(true);
                }, 500)
            }
            else if (component == 'notification' && command == 'back') {
                self.eventFormVisible(true);
                self.animateScrollDown('#create-event');
                $('#create-event').focus();
                setTimeout(function () {
                    self.notificationFormVisible(false);
                })
            }
        }

        self.animateScrollDown = function (target) {
            var target = $(target)
            $('html, body').animate({
                scrollTop: (target.offset().top - 70)
            }, 1000, "easeInOutExpo");
        }

        self.animateScrollUp = function (target) {
            var target = $(target)
            $('html, body').animate({
                scrollTop: (target.offset().top)
            }, 1000, "easeInOutExpo");
        }

        self.initUI = function () {

        }
        self.init = function () {
            //makes sure that the dom is loaded before the events trigger
            _.defer(function () {
                //self.initUI();
                //self.bindData();
                //self.initTypeAhead();
            });
        }

        self.init();
    }

    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);