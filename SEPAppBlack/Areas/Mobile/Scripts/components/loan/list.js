﻿(function (ko) {
    var componentId = "mobile-loan-list";
    var ViewModel = function (params) {
        self.customers = ko.observableArray();
        self.gridClass = ko.observable('col-lg-12 col-md-12 col-sm-12 pad-x-10');
        self.statusData = ko.observableArray([
            { name: 'Active', value: '1' },
            { name: 'Returned', value: '2' },
            { name: 'Deleted / Archived', value: '3' }
        ]);

        self.facilityData = ko.observableArray([
            { name: 'Manufacturing Facility', value: '1' },
            { name: 'Industry and Service Facility', value: '2' },
            { name: 'Health Care Facility', value: '3' },
            { name: 'Public Service Facility', value: '4' }
        ]);

        self.typeData = ko.observableArray([
            { name: 'Keys', value: '1' },
            { name: 'Passes', value: '2' },
            { name: 'Other', value: '3' }
        ]);

        self.initUI = function () {
            //Exportable table
            //$('#list-loan-grid').DataTable({
            //    dom: 'Bfrtip',
            //    responsive: true,
            //    buttons: [
            //        'copy', 'csv', 'excel', 'pdf', 'print'
            //    ]
            //});
        }

        self.bindData = function () {
            var data = [
                {
                    Id: 1111, DateCreated: '2012/04/25', Facility: 'Health Care Facility', Type: 'Keys', Name: 'Thor Walton', StartDate: '2012/04/25', ReturnedDate: '2012/06/25', 
                    Address: 'Ängsgatan 31', City: 'SVALÖV', ZipCode: '268 00', Email: 'Amirs19732822@gmail.com', MobileNumber: '4556529468942212',
                    FacilityId: 1, Item: '', Description: 'An Item that I borrowed.', GivenBy: 'Amir', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1112, DateCreated: '2012/04/25', Facility: 'Manufacturing Facility', Type: 'Keys', Name: 'Pär N Eklund', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Merry-Go-Round', Address: 'Kvillevägen 12', City: 'VÄCKELSÅNG', ZipCode: '268 00', Email: 'Ruld19971689@gmail.com', MobileNumber: '0477-7862668',
                    TypeId: 1, FacilityId: 1, Item: 'Electronic equipment', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1113, DateCreated: '2012/04/25', Facility: 'Health Care Facility', Type: 'Keys', Name: 'Sam M Lind', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Jafco', Address: 'Ängsgatan 31', City: 'SVALÖV', ZipCode: '268 00', Email: 'Amirs19732822@gmail.com', MobileNumber: '4556529468942212',
                    TypeId: 2, FacilityId: 3, Item: '', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1114, DateCreated: '2012/04/25', Facility: 'Manufacturing Facility', Type: 'Keys', Name: 'Haley Kennedy', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Merry-Go-Round', Address: 'Stackekärr 46 31', City: 'HASSELA', ZipCode: '820 78', Email: 'Toplad869358@gmail.com', MobileNumber: '0652-7432558',
                    TypeId: 1, FacilityId: 1, Item: 'Accounting Device', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1115, DateCreated: '2012/04/25', Facility: 'Industry and Service Facility', Type: 'Keys', Name: 'Folke E Sjögren', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Newhair', Address: 'Västra Husby Häggetorp 53', City: 'SLUSSFORS', ZipCode: '920 61', Email: 'Trucce9948@gmail.com', MobileNumber: '0951-3251463',
                    TypeId: 1, FacilityId: 2, Item: 'Jewelry', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1116, DateCreated: '2012/04/25', Facility: 'Health Care Facility', Type: 'Keys', Name: 'Gloria A Andreasson', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Team Uno', Address: 'Viarp 7 31', City: 'ÖSTRA KARUP', ZipCode: '269 03', Email: 'Themot19766347@gmail.com', MobileNumber: '0431-5564166',
                    TypeId: 1, FacilityId: 3, Item: 'Food cooking machine', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1117, DateCreated: '2012/04/25', Facility: 'Industry and Service Facility', Type: 'Keys', Name: 'Åke A Bergström', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Coffey', Address: 'Skolspåret 7', City: 'DALHEM', ZipCode: '620 24', Email: 'Alsetly7194@gmail.com', MobileNumber: '0498-5846676',
                    TypeId: 3, FacilityId: 2, Item: 'Dipper', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1118, DateCreated: '2012/04/25', Facility: 'Public Service Facility', Type: 'Keys', Name: 'Jonna D Sandström', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Gino', Address: 'Stackekärr 20', City: 'NORDMALING', ZipCode: '914 00', Email: 'Quishe4361@gmail.com', MobileNumber: '0930-6139621',
                    TypeId: 1, FacilityId: 4, Item: 'Appliance repairer Item', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1119, DateCreated: '2012/04/25', Facility: 'Health Care Facility', Type: 'Keys', Name: 'Elly S Lundqvist', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'Foreman & Clark Co', Address: 'Sandviken 44', City: 'ODENSBACKEN', ZipCode: '715 20', Email: 'Theken3262@gmail.com', MobileNumber: '019-6584050',
                    TypeId: 3, FacilityId: 3, Item: 'Geodetic surveyor Machine', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                },
                {
                    Id: 1120, DateCreated: '2012/04/25', Facility: 'Public Service Facility', Type: 'Keys', Name: 'Zorita Serrano', StartDate: '2012/04/25', ReturnedDate: '2012/06/25',
                    Company: 'A+ Investments', Address: 'Ängsgatan 31', City: 'SVALÖV', ZipCode: '268 00', Email: 'Amirs19732822@gmail.com', MobileNumber: '4556529468942212',
                    TypeId: 3, FacilityId: 4, Item: 'Condo UNIT', Description: 'An Item that I borrowed.', GivenBy: '', ReceivedBy: 'Ita Grossman'
                }
            ];
            self.customers = ko.observableArray(data);
        }

        self.customerDetails = function (data) {
            self.gridClass('col-lg-8 col-md-8 col-sm-8 pad-x-10');
            var param = {
                Customer: data,
                Source: 'list-preview'
            }
            _.defer(function () {
                ko.ext.renderComponent("#loan-details-container", "loan-details", param);
            })
            
        }

        self.editCusomter = function (data) {
            var param = {
                Customer: data,
                Source: 'list-edit'
            }
            _.defer(function () {
                ko.ext.renderComponent("#main-component-container", "loan-details", param);
            })
        }

        self.init = function () {
            self.initUI();
            self.bindData();
        }

        self.init();
    }

    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);