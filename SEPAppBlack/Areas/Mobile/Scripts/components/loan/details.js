﻿(function (ko) {
    var componentId = "mobile-loan-details";
    //private functions
    var ViewModel = function (params) {
        var self = this;
        self.searchCustomerEnabled = ko.observable(true);
        self.customerFieldsEnabled = ko.observable(false);
        self.editEnabled = ko.observable(false);
        self.customerData = params.Customer;
        //should get from db next
        self.facilityData = ko.observableArray([
            { name: 'Manufacturing Facility', value: '1' },
            { name: 'Industry and Service Facility', value: '2' },
            { name: 'Health Care Facility', value: '3' },
            { name: 'Public Service Facility', value: '4' },
        ]);
        //should get from db next
        self.Type = ko.observableArray([
            { name: 'Keys', value: '1' },
            { name: 'Passes', value: '2' },
            { name: 'Other', value: '3' }
        ]);

        //events
        self.existensRadioClick = function (value) {
            if (value == 0) //exists
            {
                self.searchCustomerEnabled(true);
                self.customerFieldsEnabled(false);
            }
            else //new
            {
                self.searchCustomerEnabled(false);
                self.customerFieldsEnabled(true);
            }
            return true;
        }

        self.backToList = function () {
            ko.ext.renderComponent("#main-component-container", "loan-list");
        }

        self.initUI = function () {
            if (params.Source == 'list-preview') {
                this.editEnabled(false);
            }
            else {
                this.editEnabled(true);
            }
        }

        self.init = function () {
            //makes sure that the dom is loaded before the events trigger
            _.defer(function () {
                self.initUI();
            });
        }

        self.init();
    }

    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);