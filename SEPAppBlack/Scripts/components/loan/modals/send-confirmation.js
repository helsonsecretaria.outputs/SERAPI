﻿(function (ko) {
    var componentId = "send-confirmation-details";
    var ViewModel = function (params) {
        var self = this;
        self.init = function () {

        }

        self.init();
    };
    ko.components.register(componentId, {
        viewModel: ViewModel,
        template: { element: "t-" + componentId }
    });
})(ko);