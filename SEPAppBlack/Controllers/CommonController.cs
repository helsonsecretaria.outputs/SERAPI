﻿using Newtonsoft.Json;
using SERApp.Models.Common;
using SERApp.Service.Models.ResponseModels;
using SERApp.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPAppBlack.Controllers
{
    public class CommonController : Controller
    {
        private CommonService _commonService;
        public CommonController()
        {
            _commonService = new CommonService();
        }

        [HttpGet]
        public JsonResult GetAllFacilites()
        {
            var data = _commonService.GetAllFacilites();
            var response = JsonConvert.SerializeObject(new ResponseDataModel<List<DropdownModel>>
            {
                success = true,
                message = "Successfully Loaded All Facities",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllLoanTypes()
        {
            var data = _commonService.GetAllLoanTypes();
            var response = JsonConvert.SerializeObject(new ResponseDataModel<List<DropdownModel>>
            {
                success = true,
                message = "Successfully Loaded All Loan Types",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllConfirmationTypes()
        {
            var data = _commonService.GetAllConfirmationTypes();
            var response = JsonConvert.SerializeObject(new ResponseDataModel<List<DropdownModel>>
            {
                success = true,
                message = "Successfully Loaded All Confirmation Types",
                data = data
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}