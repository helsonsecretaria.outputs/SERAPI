﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Type1Field
    {
        public int Id { get; set; }
        public int Type1Id { get; set; }
        public int FieldId { get; set; }
        public string Label { get; set; }
        public string DataSource { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public int SortOrder { get; set; }
        public string ExcludedValues { get; set; }
        public string CustomScript { get; set; }

        public DailyReportFields DailyReportField { get; set; }
    }
}
