﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public partial class DailyReportFields
    {
        public DailyReportFields()
        {

        }

        public int Id { get; set; }
        public int FieldTypeId { get; set; }
        public string DefaultLabel { get; set; }
        public string DefaultDataSource { get; set; }
        public string DefaultValue { get; set; }
        public bool DefaultIsRequired { get; set; }
        public string Type { get; set; }
        public string CustomEditorName { get; set; }
        public int SortOrder { get; set; }
        public string ControlName { get; set; }
        public string DefaultCustomScript { get; set; }
        public string ControlGroup { get; set; }

        public FieldType FieldType { get; set; }
 
        public ICollection<Type1Field> Type1Fields { get; set; }
    }
}
