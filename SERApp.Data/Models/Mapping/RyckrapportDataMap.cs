﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class RyckrapportDataMap : EntityTypeConfiguration<RyckrapportData>
    {
        public RyckrapportDataMap()
        {
            this.HasKey(t => t.RyckrapportDataId);


            // Table & Column Mappings
            this.ToTable("RyckrapportData");
            this.Property(t => t.RyckrapportDataId).HasColumnName("RyckrapportDataId");
            this.Property(t => t.ReportId).HasColumnName("ReportId");
            //this.Property(t => t.TenantId).HasColumnName("TenantId");          
            this.Property(t => t.Time).HasColumnName("Time");

            //this.HasOptional(x => x.Sections);
            this.HasRequired(x => x.Tenant);
            this.HasRequired(x => x.Report)
                .WithMany(x => x.RyckrapportDatas)
                .HasForeignKey(x => x.ReportId);
        }
    }
}
