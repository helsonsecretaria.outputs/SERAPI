﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class LogMap : EntityTypeConfiguration<Log>
    {
        public LogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Logs");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LogType).HasColumnName("LogType");
            this.Property(t => t.ShortDescription).HasColumnName("ShortDescription");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
        }
    }
}
