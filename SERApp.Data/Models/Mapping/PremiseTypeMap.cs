﻿using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class PremiseTypeMap : EntityTypeConfiguration<PremiseType>
    {
        public PremiseTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.ToTable("PremiseTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TenantTypeId).HasColumnName("TenantTypeId");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
