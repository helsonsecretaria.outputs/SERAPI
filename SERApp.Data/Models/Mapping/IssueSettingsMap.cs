﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SERApp.Data.Models.Mapping
{
    public class IssueSettingsMap : EntityTypeConfiguration<IssueSettings>
    {
        public IssueSettingsMap()
        {
            // Primary Key
            this.HasKey(t => t.IssueSettingId);

            this.ToTable("IssueSettings");
            this.Property(t => t.IssueSettingId).HasColumnName("IssueSettingId");
            this.Property(t => t.StatusInterval).HasColumnName("StatusInterval");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
        }
    }
}
