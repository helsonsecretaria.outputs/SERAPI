﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    class Type3Map : EntityTypeConfiguration<Type3>
    {
        public Type3Map()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Type3");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsTenantRequired).HasColumnName("IsTenantRequired");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.TenantTypeId).HasColumnName("TenantTypeId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");

        }
    }
}
