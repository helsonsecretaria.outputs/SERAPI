﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    public class IncidentGuardMap : EntityTypeConfiguration<IncidentGuard>
    {
        public IncidentGuardMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("IncidentGuard");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IncidentId).HasColumnName("IncidentId");
            this.Property(t => t.GuardId).HasColumnName("GuardId");
           
            // Relationships
            this.HasRequired(t => t.Incident)
                .WithMany(t => t.IncidentGuard)
                .HasForeignKey(d => d.IncidentId);
            this.HasRequired(t => t.Guard)
                .WithMany(t => t.IncidentGuard)
                .HasForeignKey(d => d.GuardId);

        }
    }
}
