﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models.Mapping
{
    class SettingMap : EntityTypeConfiguration<Setting>
    {
        public SettingMap()
        {
            // Primary Key
            this.HasKey(t => t.SettingId);

            // Table & Column Mappings
            this.ToTable("Setting");
            this.Property(t => t.SettingId).HasColumnName("SettingId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.SiteId).HasColumnName("SiteId");

            this.HasRequired(x => x.ConfigSetting).WithMany().HasForeignKey(x => x.ConfigSettingId);
        }
    }
}
