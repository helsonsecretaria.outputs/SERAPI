using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsModule { get; set; }
        public bool IsUser { get; set; }
    }
}
