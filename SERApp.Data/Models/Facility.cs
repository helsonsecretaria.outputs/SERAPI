using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class Facility
    {
        public int Id { get; set; }
        public string FacilityName { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
    }
}
