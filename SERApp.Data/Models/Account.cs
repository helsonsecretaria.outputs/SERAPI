using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class Account
    {
        public Account()
        {
            this.AccountModules = new List<AccountModule>();
            this.Sites = new List<Site>();
            this.Users = new List<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public int CompanyType { get; set; }
        public virtual ICollection<AccountModule> AccountModules { get; set; }
        public virtual ICollection<Site> Sites { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
