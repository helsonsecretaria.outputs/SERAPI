﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class TenantContact
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public int ContactId { get; set; }
        public int JobTypeId { get; set; }
    }
}
