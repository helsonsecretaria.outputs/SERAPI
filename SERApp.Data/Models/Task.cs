﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string TaskNo { get; set; }
        public string Title { get; set; }
        public string Purpose { get; set; }
        public string Description { get; set; }
        public int Interval { get; set; }
        public int NuberOfDays { get; set; }
        public int RemindAfter { get; set; }
        public int RoleId { get; set; }
        public int ExtensionsAllowed { get; set; }
        public int DayToComplete { get; set; }
        public string RecommendedRole { get; set; }
        public ICollection<TaskMainCategory> TaskMainCategory { get; set; }
        public ICollection<TaskSubCategory> TaskSubCategories { get; set; }
        public ICollection<SiteTask> SiteTask { get; set; }

        public bool IsArchived { get; set; }
        public int AccountId { get; set; }
    }
}
