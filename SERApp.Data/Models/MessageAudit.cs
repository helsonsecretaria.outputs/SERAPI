using System;
using System.Collections.Generic;

namespace SERApp.Data.Models
{
    public partial class MessageAudit
    {
        public int Id { get; set; }
        public Nullable<int> SourceId { get; set; }
        public Nullable<int> UserId { get; set; }
        public string RoutineName { get; set; }
        public string ContentSubject { get; set; }
        public string ContentBody { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string SentFrom { get; set; }
        public string SentTo { get; set; }
        public Nullable<System.DateTime> SentDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
