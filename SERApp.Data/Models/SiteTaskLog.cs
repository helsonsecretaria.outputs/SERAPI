﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERApp.Data.Models
{
    public class SiteTaskLog
    {
        public int Id { get; set; }
        public int SiteTaskId { get; set; }
        public string Comment { get; set; }
        public DateTime LogDate { get; set; }
        public int LogType { get; set; }
        public int Instance { get; set; }
        public string Owner { get; set; }
        public DateTime IntervalDate { get; set; }
        public int ExtendDay { get; set; }
        //todo add for files
    }
}
